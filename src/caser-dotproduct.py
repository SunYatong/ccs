from src.recommender.Caser import CaserRecommender
from src.data_model.SequenceDataModel import SequenceDataModel

if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 939,
        'testBatchSize': 939,
        'numFactor': 16,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 5,
        'target_length': 3,
        'hor_filter_num': 16,
        'ver_filter_num': 4,
        'dropout_keep': 0.5,
        'item_fc_dim': [32],
        'eval_item_num': 100,
        'early_stop': True,
        'random_seed': 1,
        'save_model': False,

        'long-short-interact': 'dot-product',  # 'dot-product', 'cross', 'concat-mlp', 'outproduct-conv'

        'corr-type': 'dot-product',  # 'dot-product', 'cross', 'concat-mlp', 'outproduct-conv'

        'out-product-filter-num': 2,
        'cross_layer_num': 3,

        'corr-cross-layer-num': 2,
        'corr-mlp-layer-sizes': [1],
        'corr-out-product-filter-num': 2,

        'item-weight-more': True,

    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for input_length in [5]:
            for target_length in [1]:
                for drop_keep in [0.8]:
                    for item_weight_more in [True, False]:
                        config['item-weight-more'] = item_weight_more
                        config['input_length'] = input_length
                        config['target_length'] = target_length
                        config['dropout_keep'] = drop_keep

                        dataModel.generate_sequences_hor(input_length, target_length)

                        recommender = CaserRecommender(dataModel, config)
                        recommender.run()





















