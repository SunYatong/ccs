import numpy as np
import tensorflow as tf
import random
from src.recommender.BasicRcommender import BasicRecommender
import time
from src.component.Conv import CNN_Compoment
from src.component.Capsule import Capsule_Component
from src.component import MLP

class CapSeqRecommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(CapSeqRecommender, self).__init__(dataModel, config)

        self.train_users = dataModel.train_users
        self.train_sequences_input = dataModel.train_sequences_input
        self.train_sequences_target = dataModel.train_sequences_target
        self.user_pred_sequences = dataModel.user_pred_sequences

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.name = 'CapSeq2'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.target_length = config['target_length']
        self.filter_num = config['filter_num']
        self.dropout_keep = config['dropout_keep']
        self.capsule_num = config['capsule_num']
        self.dynamic_routing_iter = config['dynamic_routing_iter']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])
        self.input_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.input_length])
        self.target_seq_pos = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [self.trainBatchSize, 100])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        self.itemBias = tf.Variable(tf.random_normal([self.numItem], 0, 0.1))

        # Conv-Capsule Layers
        self.convs = []
        self.pattern_capsules = []
        for i in range(self.input_length):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='hor'
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.filter_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.input_length - filter_height + 1,
                out_vec_len=self.numFactor,
                T=self.dynamic_routing_iter,
                name='pattern'
            )
            self.convs.append(conv)
            self.pattern_capsules.append(capsule)

        # time capsule
        self.time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                T=self.dynamic_routing_iter,
                name='time'
            )

        self.pred_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length + 2,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                T=self.dynamic_routing_iter,
                name='pred'
            )

        self.concate_size = ((self.input_length + 1) * self.input_length) / 2 + (self.numFactor * 2)
        self.fc_dim = [self.concate_size] + config['item_fc_dim']
        self.mlp = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder)

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            input_image = tf.nn.embedding_lookup(self.itemEmbedding, self.input_seq)
            input_image = tf.reshape(input_image, [-1, self.input_length, self.numFactor])
            input_image = tf.expand_dims(input_image, -1)
            # assert input_image.get_shape() == [-1, self.input_length, self.numFactor, 1]

            embeddings = []
            for i in range(self.input_length):
                filter_height = i + 1
                conv = self.convs[i].get_output(input_image)
                # assert conv.get_shape() == [-1, self.input_length-filter_height+1, 1, self.filter_num]

                conv = tf.transpose(conv, perm=[0, 3, 1, 2])
                # assert conv.get_shape() == [-1, self.filter_num, self.input_length-filter_height+1, 1]

                # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
                output = tf.reduce_sum(tf.squeeze(self.pattern_capsules[i].get_output(conv), axis=[3]), axis=1, keep_dims=False)
                # assert output.get_shape() == [-1, numFactor]
                output = tf.expand_dims(output, axis=1)
                # assert output.get_shape() == [-1, 1, numFactor]
                embeddings.append(output)

            time_output = tf.reduce_sum(tf.squeeze(self.time_capsule.get_output(input_image), axis=[3]), axis=1, keep_dims=False)
            # assert time_output.get_shape() == [-1, self.numFactor]
            time_output = tf.expand_dims(time_output, axis=1)
            # assert time_output.get_shape() == [-1, 1, self.numFactor]

            embeddings.append(time_output)

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            userEmedding = self.squash(userEmedding)
            userEmedding = tf.expand_dims(userEmedding, axis=1)
            # assert userEmedding.get_shape() == [-1, 1, self.numFactor]
            embeddings.append(userEmedding)

            concate_vec = tf.concat(values=embeddings, axis=1)
            # assert concate_vec.get_shape() == [-1, self.input_length + 2, self.numFactor]
            concate_vec = tf.expand_dims(concate_vec, axis=3)
            # assert concate_vec.get_shape() == [-1, self.input_length + 2, self.numFactor, 1]

            merged_embed = tf.reduce_sum(tf.squeeze(self.pred_capsule.get_output(concate_vec), axis=[3]), axis=1, keep_dims=False)
            # assert merged_embed.get_shape() == [-1, self.numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )

    def get_pred(self, merged_embed, ids):
        pred_embeds = tf.transpose(tf.nn.embedding_lookup(self.itemEmbedding, ids), perm=[1, 0, 2])
        pred_bias = tf.transpose(tf.nn.embedding_lookup(self.itemBias, ids))
        pred = tf.reduce_sum(merged_embed*pred_embeds, 1, keep_dims=True) + pred_bias
        return tf.transpose(pred)

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict = self.getTrainData(batchId)

        self.optimizer.run(feed_dict=feed_dict)
        loss = self.cost.eval(feed_dict=feed_dict)

        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        neg_seq_batch = []

        start_idx = batchId * self.trainBatchSize
        end_idx = start_idx + self.trainBatchSize

        if end_idx > self.trainSize:
            end_idx = self.trainSize
            start_idx = end_idx - self.trainBatchSize

        if end_idx == start_idx:
            start_idx = 0
            end_idx = start_idx + self.trainBatchSize

        user_batch = self.train_users[start_idx:end_idx]
        input_seq_batch = self.train_sequences_input[start_idx:end_idx]
        pos_seq_batch = self.train_sequences_target[start_idx:end_idx]

        for userIdx in user_batch:
            neg_items = []
            for i in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_seq_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((end_idx - start_idx, 1))
        input_seq_batch = np.array(input_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_seq_batch)

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep
        }

        return feed_dict

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_seq = []
        target_seq = []

        for userIdx in user_idices:
            input_seq.append(self.user_pred_sequences[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        input_seq = np.array(input_seq)
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.input_seq: input_seq,
            self.pred_seq: target_seq,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
