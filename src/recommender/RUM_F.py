import numpy as np
import tensorflow as tf
import random
from src.recommender.BasicRcommender import BasicRecommender
import time

class RUMFRecommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(RUMFRecommender, self).__init__(dataModel, config)

        self.train_users = dataModel.train_users
        self.train_sequences_input = dataModel.train_sequences_input
        self.train_sequences_target = dataModel.train_sequences_target
        self.user_pred_sequences = dataModel.user_pred_sequences
        self.batch_end_users = dataModel.batch_end_users

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.name = 'RUM(F)'
        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.target_length = config['target_length']
        self.dropout_keep = config['dropout_keep']
        self.loss = config['loss']
        self.feature_num = config['feature_num']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])
        self.input_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.input_length])
        self.target_seq_pos = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [self.trainBatchSize, 100])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())

        self.clear_user = tf.placeholder_with_default(False, shape=())
        self.user_clear_list = tf.placeholder(tf.int32, [None])

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        self.memory = tf.Variable(tf.random_normal([self.numUser, self.feature_num, self.numFactor], 0, 0.1))
        self.feature_key = tf.Variable(tf.random_normal([self.feature_num, self.numFactor], 0, 0.1))

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmbedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])

            neg_preds = self.get_pred(userEmbedding, self.target_seq_neg, self.target_length)
            pos_preds = self.get_pred(userEmbedding, self.target_seq_pos, self.target_length)

            if self.loss == 'bpr':
                rating_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            else:
                rating_loss = - tf.reduce_mean(tf.log(tf.nn.sigmoid(pos_preds))) - tf.reduce_mean(tf.log(1 - tf.nn.sigmoid(neg_preds)))

            self.cost = rating_loss

            self.r_pred = self.get_pred(userEmbedding, self.pred_seq, 100)

            self.write_memory(self.u_id, self.target_seq_pos)

            # clear the memroy
            zeros_dims = tf.stack([tf.shape(self.user_clear_list)[0], self.feature_num, self.numFactor])
            zeros = tf.fill(zeros_dims, 0.0)
            self.memory = tf.scatter_update(self.memory, self.user_clear_list, zeros)

    def get_pred(self, userEmbedding, target_item_ids, target_length):

        split_list = [1] * target_length
        target_item_id_list = tf.split(target_item_ids, split_list, 1)
        preds = []

        for target_item_id in target_item_id_list:
            target_itemEmbedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, target_item_id),
                                           [-1, self.numFactor])
            # [bs, self.numFactor]

            memory_out = self.read_memory(self.u_id, target_itemEmbedding)

            user_embedding_new = self.merge(userEmbedding, memory_out)

            element_wise_mul = tf.multiply(user_embedding_new, target_itemEmbedding)
            element_wise_mul_drop = tf.nn.dropout(element_wise_mul, self.dropout_keep_placeholder)

            log_intention = tf.reshape(tf.reduce_sum(element_wise_mul_drop, axis=1), [-1, 1])

            preds.append(log_intention)

        return tf.concat(preds, axis=1)

    def merge(self, u, m):
        merged = tf.add(u, tf.multiply(tf.constant(0.2), m))
        return merged

    def read_memory(self, user_id, current_item_embedding):
        # current_item_embedding = [bs, numFactor]

        memory_batch_read = tf.reshape(tf.nn.embedding_lookup(self.memory, user_id), [-1, self.feature_num, self.numFactor])
        # [bs, feature_num, numFactor]

        batch_key = tf.expand_dims(self.feature_key, axis=0)
        # [1, feature_num, numFactor]

        batch_key_tailed = tf.tile(batch_key, [self.trainBatchSize, 1, 1])
        # [bs, feature_num, numFactor]

        expanded_item_embedding = tf.expand_dims(current_item_embedding, axis=1)
        # [bs, 1, numFactor]

        dot_product = tf.multiply(batch_key_tailed, expanded_item_embedding)
        # [bs, feature_num, numFactor]

        score = tf.reduce_sum(dot_product, axis=2)
        # [bs, feature_num]

        weight = tf.nn.softmax(score)
        # [bs, feature_num]

        self.weight = tf.expand_dims(weight, axis=2)
        # [bs, feature_num, 1]

        out = tf.reduce_sum(tf.multiply(memory_batch_read, self.weight), axis=1)
        # [bs, numFactor]

        return out

    def erase(self, i):
        out = tf.nn.sigmoid(i)
        return out

    def add(self, i):
        out = tf.nn.tanh(i)
        return out

    def write_memory(self, user_id, target_item_id):
        '''

        :param user_id: [bs, 1]
        :param item_ids: [bs, target_len]
        :param len:
        :return:
        '''

        current_item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, target_item_id),
                                            [-1, self.numFactor])
        # [bs, numFactor]
        memory_batch_write = tf.reshape(tf.nn.embedding_lookup(self.memory, user_id),
                                        [-1, self.feature_num, self.numFactor])
        # [bs, feature_num, numFactor]
        ones = tf.ones([1, self.feature_num, self.numFactor], tf.float32)
        # [1, feature_num, numFactor]
        e = tf.expand_dims(self.erase(current_item_embedding), axis=1)
        # [bs, 1, numFactor]
        e = tf.tile(e, [1, self.feature_num, 1])
        # [bs, feature_num, numFactor]
        a = tf.expand_dims(self.add(current_item_embedding), axis=1)
        # [bs, 1, numFactor]
        a = tf.tile(a, [1, self.feature_num, 1])
        # [bs, feature_num, numFactor]
        decay = tf.subtract(ones, tf.multiply(self.weight, e))
        # [bs, feature_num, numFactor]
        increase = tf.multiply(self.weight, a)
        # [bs, feature_num, numFactor]
        new_value = tf.multiply(memory_batch_write, decay) + increase
        # [bs, feature_num, numFactor]

        user_id = tf.reshape(user_id, [-1])
        self.memory = tf.scatter_update(self.memory, user_id, new_value)

    def get_end_users(self, batchId):
        pass

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict_train, feed_dict_memory = self.getTrainData(batchId)

        self.optimizer.run(feed_dict=feed_dict_train)
        self.sess.run([self.memory],
                      feed_dict=feed_dict_memory)
        loss = self.cost.eval(feed_dict=feed_dict_train)

        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)

        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        neg_seq_batch = []

        start_idx = batchId * self.trainBatchSize

        if start_idx + self.trainBatchSize >= self.trainSize:
            remainder = start_idx + self.trainBatchSize - self.trainSize
            user_batch = self.train_users[start_idx:] + self.train_users[:remainder]
            input_seq_batch = self.train_sequences_input[start_idx:] + self.train_sequences_input[:remainder]
            pos_seq_batch = self.train_sequences_target[start_idx:] + self.train_sequences_target[:remainder]

        else:
            end_idx = start_idx + self.trainBatchSize
            user_batch = self.train_users[start_idx:end_idx]
            input_seq_batch = self.train_sequences_input[start_idx:end_idx]
            pos_seq_batch = self.train_sequences_target[start_idx:end_idx]

        for userIdx in user_batch:
            neg_items = []
            for i in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_seq_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((self.trainBatchSize, 1))
        input_seq_batch = np.array(input_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_seq_batch)
        user_clear_list = self.batch_end_users[batchId]

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict_train = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep,
            self.pred_seq: np.repeat(user_batch, 100, axis=1),
        }

        feed_dict_memory = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.pred_seq: np.repeat(user_batch, 100, axis=1),
            self.user_clear_list: user_clear_list
        }

        return feed_dict_train, feed_dict_memory

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_seq = []
        target_seq = []

        for userIdx in user_idices:
            input_seq.append(self.user_pred_sequences[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        input_seq = np.array(input_seq)
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.input_seq: input_seq,
            self.pred_seq: target_seq,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
