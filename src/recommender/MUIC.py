import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.Capsule import Capsule_Component
class MUIC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(MUIC_Recommender, self).__init__(dataModel, config)

        self.name = 'MUIC'
        self.user_item_capsule_num = config['user_item_capsule_num']
        self.item_item_capsule_num = config['item_item_capsule_num']
        self.preference_capsule_num = config['preference_capsule_num']
        self.capsule_lambda = config['capsule_lambda']

        # Conv-Capsule Layers
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        self.item_item_coupling = []
        self.user_item_coupling = []
        self.preference_coupling = []
        for i in range(self.numFactor):

            item_item_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.item_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('item-item-coupling-' + str(i)),
                lam=self.capsule_lambda
            )
            self.item_item_coupling.append(item_item_capsule)

            user_item_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=1 + self.input_length,
                num_caps_j=self.user_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('user-item-coupling-' + str(i)),
                lam=self.capsule_lambda
            )
            self.user_item_coupling.append(user_item_capsule)

            preference_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=1 + self.user_item_capsule_num + self.item_item_capsule_num,
                num_caps_j=self.preference_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('preference-coupling-' + str(i)),
                lam=self.capsule_lambda
            )
            self.preference_coupling.append(preference_capsule)

        # self.concate_size = self.numFactor * self.numFactor
        # self.fc_dim = [self.concate_size] + config['item_fc_dim']
        # self.mlp = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder)

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            user_matrix = self.out_product(userEmedding, userEmedding)
            user_matrix = tf.expand_dims(user_matrix, 1)

            item_matrices = []

            split_list = [1] * self.input_length
            itemIds = tf.split(self.input_seq, split_list, 1)

            for itemId in itemIds:
                item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
                item_matrix = self.out_product(item_embedding, item_embedding)
                # [bs, numFactor, numFactor]
                item_matrix = tf.expand_dims(item_matrix, 1)
                item_matrices.append(item_matrix)
                # [bs, 1, numFactor, numFactor] * seq_len

            item_item_inputs = tf.concat(item_matrices, axis=1)
            # [bs, seq_len numFactor, numFactor]
            user_item_inputs = tf.concat(item_matrices + [user_matrix], axis=1)

            matrix_split_list = [1] * self.numFactor
            item_item_inputs = tf.split(item_item_inputs, matrix_split_list, 3)
            # [bs, seq_len, numFactor, 1] * numFactor
            user_item_inputs = tf.split(user_item_inputs, matrix_split_list, 3)
            # [bs, seq_len+1, numFactor, 1] * numFactor

            user_matrix_slices = tf.split(user_matrix, matrix_split_list, 3)
            # [bs, 1, numFactor, 1] * numFactor

            preference_outputs = []
            for capsule_idx in range(self.numFactor):
                user_item_capsule = self.user_item_coupling[capsule_idx]
                # each capsule input : [bs, seq_len, numFactor, 1] -> [bs, num_capsule, capsule_len, 1]
                user_item_output = user_item_capsule.get_output(user_item_inputs[capsule_idx], None)
                # [bs, num_j_capsule=num_capsule, vec_len=numFactor, 1]

                item_item_capsule = self.item_item_coupling[capsule_idx]
                item_item_output = item_item_capsule.get_output(item_item_inputs[capsule_idx], None)
                # [bs, num_capsule, numFactor, 1]

                user_matrix_slice = user_matrix_slices[capsule_idx]
                # [bs, 1, numFactor, 1]

                preference_input = tf.concat([user_item_output, item_item_output, user_matrix_slice], 1)
                # [bs, 1 + num_capsule*2, numFactor, 1]

                preference_capsule = self.preference_coupling[capsule_idx]
                preference_output = preference_capsule.get_output(preference_input, None)
                # [bs, num_capsule, numFactor, 1]

                preference_outputs.append(preference_output)

            preference_matrices = tf.concat(preference_outputs, 3)
            # [bs, num_capsule, numFactor, numFactor]
            preference_embed = tf.reduce_sum(preference_matrices, axis=3, keepdims=False)
            # [bs, num_capsule, numFactor]
            merged_embed = tf.reduce_sum(preference_embed, axis=1, keepdims=False)
            # [bs, numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )
