import tensorflow as tf
from src.recommender.CapSeq_Basic import CapSeqBasic
from src.component.Conv import CNN_Compoment
from src.component.Capsule import Capsule_Component
from src.component import MLP

class CapSeq_FC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(CapSeq_FC_Recommender, self).__init__(dataModel, config)

        self.name = 'CapSeq_FC_Recommender-no-time-user'

        # Conv-Capsule Layers
        self.hor_convs = []
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        self.filter_size_num = self.input_length - 2
        self.num_capsule_1 = config['capsule_num_1']
        self.num_capsule_2 = config['capsule_num_2']
        self.num_capsule_time = config['capsule_num_time']
        for i in range(self.filter_size_num):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='hor'
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length - filter_height + 1,
                num_caps_j=config['capsule_num_1'],
                in_vec_len=self.filter_num,
                out_vec_len=self.filter_num,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name=str('pattern-' + str(filter_height))
            )
            self.hor_convs.append(conv)
            self.hor_pattern_capsules.append(capsule)

        self.output_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=config['capsule_num_1'] * self.filter_size_num,
                num_caps_j=config['capsule_num_2'],
                in_vec_len=self.filter_num,
                out_vec_len=self.filter_num,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='output'
            )

        # time capsule
        self.time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=config['capsule_num_time'],
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='time'
            )

        self.concate_size = config['capsule_num_2'] * self.filter_num + self.numFactor

        self.fc_dim = [self.concate_size] + config['item_fc_dim']
        self.mlp = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder)

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])

            input_image = tf.nn.embedding_lookup(self.itemEmbedding, self.input_seq)
            input_image = tf.reshape(input_image, [-1, self.input_length, self.numFactor])
            input_image = tf.expand_dims(input_image, -1)
            # assert input_image.get_shape() == [-1, self.input_length, self.numFactor, 1]

            embeddings = []
            for i in range(self.filter_size_num):

                conv = self.hor_convs[i].get_output(input_image)
                # assert conv.get_shape() == [-1, L-K+1, 1, numFilter]

                conv = tf.transpose(conv, perm=[0, 1, 3, 2])
                # assert conv.get_shape() == [-1, L-K+1, numFilter, 1]
                # [bs, numCap_i, vec_len, 1]

                # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
                output = self.hor_pattern_capsules[i].get_output(conv, userEmedding)

                embeddings.append(output)

            concate_capsule_output = tf.concat(values=embeddings, axis=1)
            self.cap_output = tf.Print(input_=concate_capsule_output, data=[concate_capsule_output], message="cap_output",
                                                   summarize=self.concate_size * 5)
            # assert concate_vec.get_shape() == [batch_size, num_capsule * filter_size_num, output_vec_len, 1]

            cap_output = self.output_capsule.get_output(concate_capsule_output, None)
            # assert cap_output.get_shape() == [batch_size, num_capsule, output_vec_len, 1]

            flatten = tf.reshape(cap_output, [-1, self.num_capsule_2 * self.filter_num])

            #time_output = self.time_capsule.get_output(input_image, userEmedding)
            #time_output = tf.reshape(time_output, [-1, self.num_capsule_time * self.numFactor])

            user_flatten = tf.concat(values=[flatten, userEmedding], axis=1)
            # assert flatten.get_shape() == [batch_size, self.capsule_num * self.filter_num]

            merged_embed = self.mlp.get_output(feature_input=user_flatten)
            # assert merged_embed.get_shape() == [-1, self.numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=self.eval_item_num
            )

