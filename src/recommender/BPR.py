import numpy as np
import tensorflow as tf
import random
from recommender.BasicRcommender import BasicRecommender
import time

class BPRRecommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(BPRRecommender, self).__init__(dataModel, config)

        self.name = 'BPRRecommender'

        self.numFactor = config['numFactor']
        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.i_id = tf.placeholder(tf.int32, [None, 1])
        self.j_id = tf.placeholder(tf.int32, [None, 1])
        self.factor_lambda = config['factor_lambda']

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        # self.itemBias = tf.Variable(tf.random_normal([self.numItem, 1], 0, 0.1))

        self.sampleSize = self.trainSize

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:
            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            itemEmedding_i = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.i_id), [-1, self.numFactor])
            itemEmedding_j = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.j_id), [-1, self.numFactor])
            # bias_i = tf.reshape(tf.nn.embedding_lookup(self.itemBias, self.i_id), [-1, 1])
            # bias_j = tf.reshape(tf.nn.embedding_lookup(self.itemBias, self.j_id), [-1, 1])

            pred_i = tf.reduce_sum(tf.multiply(userEmedding, itemEmedding_i), 1, keep_dims=True) #+ bias_i
            pred_j = tf.reduce_sum(tf.multiply(userEmedding, itemEmedding_j), 1, keep_dims=True) #+ bias_j

            predDiff = pred_i - pred_j

            l2_norm = self.factor_lambda * tf.nn.l2_loss(userEmedding) \
                     + self.factor_lambda * tf.nn.l2_loss(itemEmedding_i) \
                     + self.factor_lambda * tf.nn.l2_loss(itemEmedding_j)

            self.r_pred = pred_i
            bprLoss = - tf.reduce_sum(tf.log(tf.sigmoid(predDiff)))
            self.cost = bprLoss + l2_norm

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        batch_u, batch_i, batch_j = self.getTrainData(batchId)

        self.optimizer.run(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_i,
            self.j_id: batch_j
        })
        loss = self.cost.eval(feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_i,
            self.j_id: batch_j
        })
        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
            "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss


    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        user_batch = []
        pos_item_batch = []
        neg_item_batch = []
        for i in range(self.trainBatchSize):
            userIdx = random.randint(0, self.numUser - 2)
            positiveItems = self.user_items_train[userIdx]
            positiveItemIdx = positiveItems[random.randint(0, len(positiveItems) - 1)]
            negativeItemIdx = random.randint(0, self.numItem - 1)
            while negativeItemIdx in positiveItems:
                negativeItemIdx = random.randint(0, self.numItem - 1)

            user_batch.append(userIdx)
            pos_item_batch.append(positiveItemIdx)
            neg_item_batch.append(negativeItemIdx)

        batch_u = np.array(user_batch).reshape((self.trainBatchSize, 1))
        batch_i = np.array(pos_item_batch).reshape((self.trainBatchSize, 1))
        batch_j = np.array(neg_item_batch).reshape((self.trainBatchSize, 1))

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")


        return batch_u, batch_i, batch_j

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        testBatch_user = []

        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            for itemIdx in self.evalItemsForEachUser[userIdx]:
                testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)
        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(start, end):
                recommendList[batch_v[j][0]] = predList[j][0]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2

    def getPredList_ByUserIdx(self, userIdx):
        # build test batch
        testBatch_user = []

        for itemIdx in self.evalItemsForEachUser[userIdx]:
            testBatch_user.append([userIdx, itemIdx, 1.0])

        batch_u = np.array(testBatch_user)[:, 0:1].astype(np.int32)
        batch_v = np.array(testBatch_user)[:, 1:2].astype(np.int32)

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.i_id: batch_v,
        })

        recommendList = {}
        for i in range(len(testBatch_user)):
            recommendList[batch_v[i][0]] = predList[i][0]

        sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]

        return sorted_RecItemList