import numpy as np
import tensorflow as tf
import random
from recommender.BasicRcommender import BasicRecommender
import time


class CapSeqBasic(BasicRecommender):

    def __init__(self, dataModel, config):

        super(CapSeqBasic, self).__init__(dataModel, config)

        self.train_users = dataModel.train_users
        self.train_sequences_input = dataModel.train_sequences_input
        self.train_sequences_target = dataModel.train_sequences_target
        self.user_pred_sequences = dataModel.user_pred_sequences
        self.user_item_num = dataModel.user_item_num

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.name = 'CapSeqBasic'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.target_length = config['target_length']
        self.filter_num = config['filter_num']
        self.dropout_keep = config['dropout_keep']
        self.capsule_num = config['capsule_num']
        self.dynamic_routing_iter = config['dynamic_routing_iter']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])
        self.input_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.input_length])
        self.target_seq_pos = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.eval_item_num])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())
        self.is_train = tf.placeholder_with_default(True, shape=())

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        self.itemBias = tf.Variable(tf.random_normal([self.numItem], 0, 0.1))

        self.cap_output = None

        self.cluster_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])

        # self.numCluster = config['num_cluster']
        # feature_matrix = np.loadtxt('./save_model/' + 'ml-100k' + '-' + 'BPRRecommender' + '-user_embed.txt')
        # cluster = My_KMeans(feature_matrix, num_cluster=self.numCluster)
        # cluster.build_cluster()
        # self.user_to_cluster = cluster.u_cluster_labels

        # self.train_clusters = self.getTrainClusters()
        # if config['cluster_pretrain']:
        #     self.user_cluster_Embedding = tf.Variable(initial_value=cluster.means, trainable=False, dtype=tf.float32)
        # else:
        # self.user_cluster_Embedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))

        self.reset_plh = tf.placeholder(tf.float32, [self.trainBatchSize, self.numFactor])

    def get_pred(self, merged_embed, ids):
        pred_embeds = tf.transpose(tf.nn.embedding_lookup(self.itemEmbedding, ids), perm=[1, 0, 2])
        pred_bias = tf.transpose(tf.nn.embedding_lookup(self.itemBias, ids))
        pred = tf.reduce_sum(merged_embed*pred_embeds, 1, keep_dims=True) + pred_bias
        return tf.transpose(pred)

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict = self.getTrainData(batchId)

        self.optimizer.run(feed_dict=feed_dict)
        loss = self.cost.eval(feed_dict=feed_dict)

        if self.config['verbose'] and batchId % 10 == 0:
            self.cap_output.eval(feed_dict=feed_dict)

        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)

        return totalLoss

    # def getTrainClusters(self):
    #
    #     train_clusters = []
    #     for userIdx in self.train_users:
    #         clusterIdx = self.user_to_cluster[userIdx]
    #         train_clusters.append(clusterIdx)
    #     return train_clusters

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        neg_seq_batch = []
        reset_mat = np.ones([self.trainBatchSize, self.numFactor])

        start_idx = batchId * self.trainBatchSize
        end_idx = start_idx + self.trainBatchSize

        if end_idx > self.trainSize:
            end_idx = self.trainSize
            start_idx = end_idx - self.trainBatchSize

        if end_idx == start_idx:
            start_idx = 0
            end_idx = start_idx + self.trainBatchSize

        user_batch = self.train_users[start_idx:end_idx]
        for i in range(len(user_batch)):
            userIdx = user_batch[i]
            if self.user_item_num[userIdx] < self.config['rating_threshold']:
                reset_mat[i] = 0
        # lazy operate
        cluster_batch = [1] * self.trainBatchSize
        input_seq_batch = self.train_sequences_input[start_idx:end_idx]
        pos_seq_batch = self.train_sequences_target[start_idx:end_idx]

        for idx in range(len(user_batch)):
            neg_items = []
            for userIdx in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_seq_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((end_idx - start_idx, 1))
        cluster_batch = np.array(cluster_batch).reshape((end_idx - start_idx, 1))
        input_seq_batch = np.array(input_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_seq_batch)

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict = {
            self.u_id: user_batch,
            self.cluster_id: cluster_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep,
            self.is_train: True,
            self.reset_plh: reset_mat
        }

        return feed_dict

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_seq = []
        cluster_seq = []
        target_seq = []

        for userIdx in user_idices:
            cluster_seq.append(1)
            input_seq.append(self.user_pred_sequences[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        reset_mat = np.ones([len(user_idices), self.numFactor])

        count = 0
        for userIdx in user_idices:
            if self.user_item_num[userIdx] < self.config['rating_threshold']:
                reset_mat[count] = 0
            count += 1

        cluster_seq = np.array(cluster_seq).reshape((-1, 1))
        input_seq = np.array(input_seq)
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.cluster_id: cluster_seq,
            self.input_seq: input_seq,
            self.pred_seq: target_seq,
            self.reset_plh: reset_mat,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * self.eval_item_num
            end = start + self.eval_item_num
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
