import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.Conv import CNN_Compoment
from component.CapsuleEM import EM_Capsule_Component
from component.Capsule import Capsule_Component
from component.Conv_Pool import CNN_Pool_Compoment

from src.component.MLP import MLP

class CapSeq_Conv_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(CapSeq_Conv_Recommender, self).__init__(dataModel, config)

        self.name = 'CapSeq_Conv'

        # Conv-Capsule Layers
        self.hor_convs = []
        self.em_capsules = []
        self.user_embeddings = []
        self.filter_size_num = self.input_length - 2
        self.num_capsule_1 = config['capsule_num_1']
        self.num_capsule_2 = config['capsule_num_2']
        self.num_capsule_3 = config['capsule_num_3']
        self.capsule_lambda = config['capsule_lambda']
        self.pose_len = config['pose_len']
        self.config = config
        self.mlp_size = 0

        self.item_fc_dim_pool = [self.filter_num * self.input_length] + config['item_fc_dim']

        for i in range(self.filter_size_num):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='hor',
                cnn_lambda=config['cnn_lam']
            )
            capsule = EM_Capsule_Component(
                bs=self.trainBatchSize,
                T=self.dynamic_routing_iter,
                wordVec_len=self.filter_num,
                filter_height_1=1,
                filter_height_2=2,
                filter_height_3=2,
                num_caps_b=self.num_capsule_1,
                num_caps_c=self.num_capsule_2,
                num_caps_d=self.num_capsule_3,
                pose_len=self.pose_len,
                user_vec_len=self.numFactor,
                user_bias=True,
                name=str('pattern-' + str(filter_height)),
                layer_num=2,
                lam=config['capsule_lam'],
                routing_type=config['routing_type']
            )
            self.mlp_size += capsule.get_output_shape(input_height=self.input_length, filter_height=filter_height)
            self.hor_convs.append(conv)
            self.em_capsules.append(capsule)

        # time capsule
        if self.config['add_time']:
            self.time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='time'
            )
            self.concate_size = self.mlp_size + (self.numFactor * (self.capsule_num + 1))
        else:
            self.concate_size = self.mlp_size + self.numFactor

        self.horizontal_CNN = CNN_Pool_Compoment(
            filter_num=self.filter_num,
            filter_sizes=[i+1 for i in range(self.input_length)],
            wordvec_size=self.numFactor,
            max_review_length=self.input_length,
            word_matrix=self.itemEmbedding,
            output_size=1,
            review_wordId_print=None,
            review_input_print=None,
            cnn_lambda=None,
            dropout_keep_prob=None,
            component_raw_output=None,
            item_pad_num=None,
            name='hor'
        )
        self.concate_size += self.filter_num * self.input_length

        self.fc_dim = [self.concate_size] + config['item_fc_dim']
        self.mlp = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder, lam=config['mlp_lam'])




    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            user_for_capsules = tf.multiply(userEmedding, self.reset_plh)
            # userClusterEmedding = tf.reshape(tf.nn.embedding_lookup(self.user_cluster_Embedding, self.cluster_id), [-1, self.numFactor])

            input_image = tf.nn.embedding_lookup(self.itemEmbedding, self.input_seq)
            input_image = tf.reshape(input_image, [-1, self.input_length, self.numFactor])
            input_image = tf.expand_dims(input_image, -1)
            # assert input_image.get_shape() == [-1, self.input_length, self.numFactor, 1]

            hor_embed = self.horizontal_CNN.get_horizontal_output(self.input_seq)

            embeddings = []
            for i in range(self.filter_size_num):

                conv = self.hor_convs[i].get_output(input_image)
                # assert conv.get_shape() == [-1, L-K+1, 1, numFilter]

                conv = tf.transpose(conv, perm=[0, 1, 3, 2])
                # assert conv.get_shape() == [-1, L-K+1, numFilter, 1]
                # [bs, numCap_i, vec_len, 1]

                # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
                pose, activation = self.em_capsules[i].get_output(conv, self.is_train, user_for_capsules,
                                                                  at_b=self.config['at_b'],
                                                                  at_c=self.config['at_c'],
                                                                  share_user_para_b=self.config['share_user_para_b'],
                                                                  share_user_para_c=self.config['share_user_para_c'],
                                                                  user_as_miu_bias=self.config['user_as_miu_bias'],
                                                                  user_reg_cost=self.config['user_reg_cost'])

                if activation != None:
                    output = pose * activation
                else:
                    output = pose

                # [self.bs, data_height, self.num_caps_c, self.pose_len]
                output_shape = output.get_shape()

                output = tf.reshape(output, shape=[-1, output_shape[1] * output_shape[2] * output_shape[3]])

                embeddings.append(output)

            concate_capsule_output = tf.concat(values=embeddings, axis=1)

            user_flatten = tf.concat(values=[concate_capsule_output, hor_embed, userEmedding], axis=1)
            # assert flatten.get_shape() == [batch_size, self.capsule_num * self.filter_num]
            merged_embed = self.mlp.get_output(feature_input=user_flatten)
            # assert merged_embed.get_shape() == [-1, self.numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )
