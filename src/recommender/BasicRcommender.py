import numpy as np
import tensorflow as tf
import time
from eval.RankingEvaluator import RankingEvaluator
import os
import random
from component.CrossNetwork import CrossNetwork
from component.Conv_Pool import CNN_Pool_Compoment
from component import MLP

epsilon = 1e-9

class BasicRecommender:

    def __init__(self, dataModel, config):

        self.config = config
        self.name = 'BasicRecommender'
        tf.set_random_seed(config['random_seed'])
        random.seed(config['random_seed'])

        self.numFactor = config['numFactor']
        self.item_fc_dim = []
        self.trainSet = np.array(dataModel.trainSet)
        self.testSet = np.array(dataModel.testSet)
        self.testMatrix = dataModel.buildTestMatrix()
        self.trainSize = len(dataModel.trainSet)
        self.testSize = len(dataModel.testSet)
        self.numUser = dataModel.numUser
        self.numItem = dataModel.numItem
        self.numWord = dataModel.numWord
        self.evalItemsForEachUser = dataModel.evalItemsForEachUser
        self.userIdxToUserId = dataModel.userIdxToUserId
        self.itemIdxToItemId = dataModel.itemIdxToItemId
        self.userIdToUserIdx = dataModel.userIdToUserIdx
        self.itemIdToItemIdx = dataModel.itemIdToItemIdx

        self.user_items_train = dataModel.user_items_train
        self.user_items_train_paded = dataModel.user_items_train_paded
        self.user_items_test = dataModel.user_items_test
        self.itemsInTestSet = dataModel.itemsInTestSet
        self.fileName = dataModel.fileName
        self.logger = dataModel.logger

        self.r_pred = None
        self.r_label = None
        self.cost = tf.constant(0.0)
        self.min_loss = np.PINF
        self.loss_increas_count = 0
        self.auc = None
        self.precision = None

        self.best_NDCG = 0
        self.best_NDCG_BatchId = 0
        self.best_NDCG_EpochId = 0

        self.best_AUC = 0
        self.best_AUC_BatchId = 0
        self.best_AUC_EpochId = 0

        self.best_Precision = 0
        self.best_Precision_BatchId = 0
        self.best_Precision_EpochId = 0

        self.best_Recall = 0
        self.best_Recall_BatchId = 0
        self.best_Recall_EpochId = 0

        self.bestNDCG = 0
        self.bestNDCGBatchId = 0
        self.bestNDCGEpochId = 0

        self.bestNDCG = 0
        self.bestNDCGBatchId = 0
        self.bestNDCGEpochId = 0

        self.bestRMSE = 100
        self.bestRatingMetricBatchId = 0
        self.bestRatingMetrixEpochId = 0

        self.optimizer = None
        self.sess = None
        self.seed = 123

        self.last_loss = 0
        self.learn_stop_count = 0
        self.dropout_keep_placeholder = 0.8

        self.fileName = config['fileName']
        self.outputPath = './dataset/processed_datasets'

        self.learnRate = config['learnRate']
        self.maxIter = config['maxIter']
        self.trainBatchSize = config['trainBatchSize']
        self.testBatchSize = config['testBatchSize']
        self.topN = config['topN']
        self.goal = config['goal']
        self.eval_item_num = config['eval_item_num']
        self.early_stop = config['early_stop']

        if self.trainSize % self.trainBatchSize == 0:
            self.trainBatchNum = int(self.trainSize // self.trainBatchSize)
        else:
            self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        if self.testSize % self.testBatchSize == 0:
            self.testBatchNum = int(self.testSize // self.testBatchSize)
        else:
            self.testBatchNum = int(self.testSize // self.testBatchSize) + 1

        self.evalRanking = RankingEvaluator(groundTruthLists=self.user_items_test,
                                                                      user_items_train=self.user_items_train,
                                                                      itemInTestSet=self.itemsInTestSet,
                                                                      topK=self.topN,
                                                                      testMatrix=self.testMatrix)
        # self.evalRating = eval.RatingEvaluator.RatingEvaluator(r_label=self.testSet[:, 2:3])
        self.eval_user_lists = self.generate_eval_user_lists()

        # user/item embedding
        self.userEmbedding = None
        self.itemEmbedding = None
        self.corr_network = None

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))

        # item embeddings for prediction
        if self.config['item-weight-more']:
            self.output_fc_W = tf.get_variable(
                name="output_fc_W",
                dtype=tf.float32,
                shape=[self.numItem, self.numFactor],
                initializer=tf.contrib.layers.xavier_initializer()
            )
        else:
            self.output_fc_W = self.itemEmbedding


    def pred_for_a_user(self, W, b, ids, numFactor, input_feature, tar_length):

        split_list = [1] * tar_length
        itemIds = tf.split(ids, split_list, 1)
        preds = []

        for itemId in itemIds:

            item_embedding = tf.reshape(tf.nn.embedding_lookup(W, itemId), [-1, numFactor])
            item_bias = tf.reshape(tf.nn.embedding_lookup(b, itemId), [-1, 1])

            pred = self.correlation(user_embed=input_feature, item_embed=item_embedding, corr_type=self.config['corr-type']) + item_bias

            preds.append(pred)

        return tf.concat(preds, axis=1)

    def pred_for_a_user_broad(self, W, b, ids, numFactor, input_feature, tar_length):

        item_embeddings = tf.reshape(tf.nn.embedding_lookup(W, ids), [-1, tar_length, numFactor])
        item_biases = tf.reshape(tf.nn.embedding_lookup(b, ids), [-1, tar_length])

        preds = self.correlations(user_embed=input_feature, item_embeds=item_embeddings, corr_type=self.config['corr-type']) + item_biases

        return preds



    def pred_for_a_user_no_bias(self, W, ids, numFactor, input_feature, tar_length):

        split_list = [1] * tar_length
        itemIds = tf.split(ids, split_list, 1)
        preds = []

        for itemId in itemIds:

            item_embedding = tf.reshape(tf.nn.embedding_lookup(W, itemId), [-1, numFactor])

            pred = self.correlation(user_embed=input_feature, item_embed=item_embedding, corr_type=self.config['corr-type'])

            preds.append(pred)

        return tf.concat(preds, axis=1)

    def pred_for_a_user_item(self, itemEmbedding, itemBias, numFactor, user_input_feature, itemId):

        item_embeddings = tf.reshape(tf.nn.embedding_lookup(itemEmbedding, itemId), [-1, numFactor])
        item_bias = tf.reshape(tf.nn.embedding_lookup(itemBias, itemId), [-1, 1])
        dotproduct = tf.multiply(item_embeddings, user_input_feature)
        dotproduct = tf.reshape(dotproduct, shape=[-1, numFactor])
        pred = tf.reduce_sum(dotproduct, 1, keep_dims=True) + item_bias

        return pred



    def buildModel(self):
        pass

    def trainModel(self):
        self.sess = tf.InteractiveSession()

        self.optimizer = tf.train.AdamOptimizer(self.learnRate, name='Adam_optimizer').minimize(self.cost)

        self.sess.run(tf.global_variables_initializer())
        for epochId in range(self.maxIter):
            start = time.time()
            totalLoss = 0
            for batchId in range(self.trainBatchNum):
                loss = self.trainEachBatch(epochId, batchId)
                totalLoss += loss
            end = time.time()
            self.logger.info("time cost of an epoch:" + str(end - start) + ", totalLoss: " + str(totalLoss))

            if np.isnan(totalLoss):
                self.logger.info("the loss is nan, training stopped.")
                break
            if totalLoss < self.min_loss:
                self.min_loss = totalLoss
            # when the loss doesn't decrease, stop
            if self.early_stop:
                if self.loss_increas_count > 100:
                    break
                # when the performance doesn't increase, stop
                if epochId - self.best_AUC_EpochId > 100 and epochId - self.best_NDCG_EpochId > 100:
                    break

        tf.reset_default_graph()


    def trainEachBatch(self, epochId, batchId):
        pass

    def run(self):
        self.printInfo()
        self.buildModel()
        self.trainModel()

    def getTrainData(self, batchId):
        pass

    def generate_eval_user_lists(self):
        eval_user_lists = []
        test_user_list = list(self.user_items_test.keys())
        idx_range = len(test_user_list)

        if idx_range % self.testBatchSize == 0:
            step_num = idx_range // self.testBatchSize
        else:
            step_num = idx_range // self.testBatchSize + 1

        for i in range(step_num):
            start = self.testBatchSize * i
            end = start + self.testBatchSize
            if end > idx_range:
                end = idx_range
            user_idices = test_user_list[start:end]
            eval_user_lists.append(user_idices)
        return eval_user_lists

    def evaluateRanking(self, epochId, batchId):

        userPredLists = {}
        start = time.time()

        packTime_total = 0
        runTime_total = 0
        sortTime_total = 0

        for user_list in self.eval_user_lists:
            user_pred_lists, packTime, runTime, sortTime = self.getPredList_ByUserIdxList(user_list)
            packTime_total += packTime
            runTime_total += runTime
            sortTime_total += sortTime
            for i in range(len(user_list)):
                userIdx = user_list[i]
                pred_list = user_pred_lists[i]
                userPredLists[userIdx] = pred_list

        end = time.time()

        self.logger.info("generate recList time cost: %.4f" % (end - start))
        self.logger.info("packTime: %.4f, runTime: %.4f, sortTime: %.4f" %
                         (packTime_total, runTime_total, sortTime_total))
        self.evalRanking.setPredLists(userPredLists)
        newNDCG, ndcgTime = self.evalRanking.calNDCG()
        newAUC, aucTime = self.evalRanking.calAUC()
        newPrecision, precisionTime = self.evalRanking.calPrecision()
        newRecall, recallTime = self.evalRanking.calRecall()

        self.logger.info("Recall: " + str([newRecall, recallTime]))
        self.logger.info("Precision: " + str([newPrecision, precisionTime]))
        self.logger.info("AUC: " + str([newAUC, aucTime]))
        self.logger.info("NDCG: " + str([newNDCG, ndcgTime]))

        self.saveBestResult(newNDCG=newNDCG,
                            newAUC=newAUC,
                            newPrecision=newPrecision,
                            newRecall=newRecall,
                            epochId=epochId,
                            batchId=batchId)

        self.showBestRankingResult()
        # self.logger.info('saving results of epoch:' + str(epochId) + ', batchId:' + str(batchId))
        # self.printRankResult()
        # self.logger.info('saving results finished')

    def saveBestResult(self, newNDCG, newAUC, newPrecision, newRecall, epochId, batchId):

        if newNDCG > self.best_NDCG:
            self.best_NDCG = newNDCG
            self.best_NDCG_EpochId = epochId
            self.best_NDCG_BatchId = batchId

        if newAUC > self.best_AUC:
            self.best_AUC = newAUC
            self.best_AUC_EpochId = epochId
            self.best_AUC_BatchId = batchId
            if self.config['save_model']:
                self.saveWeight()

        if newPrecision > self.best_Precision:
            self.best_Precision = newPrecision
            self.best_Precision_EpochId = epochId
            self.best_Precision_BatchId = batchId

        if newRecall > self.best_Recall:
            self.best_Recall = newRecall
            self.best_Recall_EpochId = epochId
            self.best_Recall_BatchId = batchId

    def saveWeight(self):
        np.savetxt('./save_model/' + self.config['fileName'] + '-' + self.name + '-user_embed.txt', self.userEmbedding.eval())
        np.savetxt('./save_model/' + self.config['fileName'] + '-' + self.name + '-item_embed.txt', self.itemEmbedding.eval())

    def evaluateRating(self, epochId, batchId):
        start = time.time()
        r_pred = self.getRatingPredictions()
        end = time.time()
        self.logger.info("pred time cost: " + str(end - start))

        self.evalRating.set_r_pred(r_pred)
        rmse, mae, timeCost = self.evalRating.cal_RMSE_and_MAE()
        self.logger.info("(RMSE, MAE, EvalTimeCost)="+str([rmse, mae, timeCost]))

        if rmse < self.bestRMSE:
            self.bestRMSE = rmse
            self.bestRatingMetrixEpochId = epochId
            self.bestRatingMetricBatchId = batchId
        self.showBestRatingResult()

    def printRankResult(self):
        outputLines = []
        for userIdx in self.user_items_test:
            eachLine = ''
            userId = self.userIdxToUserId[userIdx]
            eachLine += (str(userId) + ':')
            itemidices = self.getPredList_ByUserIdx(userIdx)
            for itemIdx in itemidices:
                itemId = self.itemIdxToItemId[itemIdx]
                eachLine += (str(itemId) + ' ')
            eachLine += '\n'
            outputLines.append(eachLine)

        fullOutputPath = self.outputPath + '/' + self.fileName + '/userTimeRatio'
        with open(fullOutputPath + '/result.txt', 'w') as resultFile:
            resultFile.writelines(outputLines)

    def getRatingPredictions(self):
        pass

    def getPredList_ByUserIdx(self, userIdx):
        pass

    def getPredList_ByUserIdxList(self, userIdx):
        pass

    def getTestData(self):
        pass

    def showBestRankingResult(self):
        self.logger.info("best Precision result: %.4f, batchId: %d, epochId: %d" %
                         (self.best_Precision, self.best_Precision_BatchId, self.best_Precision_EpochId))
        self.logger.info("best Recall result: %.4f, batchId: %d, epochId: %d" %
                         (self.best_Recall, self.best_Recall_BatchId, self.best_Recall_EpochId))
        self.logger.info("best AUC result: %.4f, batchId: %d, epochId: %d" %
                         (self.best_AUC, self.best_AUC_BatchId, self.best_AUC_EpochId))
        self.logger.info("best NDCG result: %.4f, batchId: %d, epochId: %d" %
                         (self.best_NDCG, self.best_NDCG_BatchId, self.best_NDCG_EpochId))

    def showBestRatingResult(self):
        self.logger.info("best RMSE result: RMSE:" + str(self.bestRMSE) + ", batchId: " + str(self.bestRatingMetricBatchId) + ", epochId: " + str(self.bestRatingMetrixEpochId))

    def printInfo(self):
        self.logger.info("\n###### Recommender Info #########\n")
        self.logger.info("Name: %s" % (self.name))
        self.logger.info("num core: %d" % (os.cpu_count()))
        for key, value in self.config.items():
            self.logger.info("%s = %s" % (str(key), str(self.config[key])))

    def sigmoid(self, x):
        """
        Compute the sigmoid of x

        Arguments:
        x -- A scalar or numpy array of any size

        Return:
        s -- sigmoid(x)
        """

        ### START CODE HERE ### (≈ 1 line of code)
        s = 1.0 / (1.0 + np.exp(-x))
        ### END CODE HERE ###

        return s

    def activ(self, name, tensor):
        if name == 'sigmoid':
            return tf.nn.sigmoid(tensor)
        elif name == 'relu':
            return tf.nn.relu(tensor)
        elif name == 'tanh':
            return tf.nn.tanh(tensor)
        else:
            return tensor

    def squash(self, vector):
        '''Squashing function corresponding to Eq. 1
        Args:
            vector: A tensor with shape [batch_size, vec_len].
        Returns:
            A tensor with the same shape as vector.
        '''
        vec_squared_norm = tf.reduce_sum(tf.square(vector), -1, keep_dims=True)
        scalar_factor = vec_squared_norm / (1 + vec_squared_norm) / tf.sqrt(vec_squared_norm + epsilon)
        vec_squashed = tf.multiply(vector, scalar_factor)  # element-wise
        return (vec_squashed)

    def out_product(self, embed_ver, embed_hor):
        embed_ver = tf.expand_dims(embed_ver, axis=2)  # mxdx1
        embed_hor = tf.expand_dims(embed_hor, axis=2)  # mxdx1
        hor_size = embed_hor.get_shape().as_list()[1]
        ver_size = embed_ver.get_shape().as_list()[1]
        embed_ver_broad_horizon = tf.tile(embed_ver, [1, 1, hor_size])
        embed_hor_broad_vertical = tf.transpose(tf.tile(embed_hor, [1, 1, ver_size]), [0, 2, 1])
        out_product = tf.multiply(embed_ver_broad_horizon, embed_hor_broad_vertical)

        return out_product

    def long_short_interact(self, long_embed, short_embed):
        if self.config['long-short-interact'] == 'dot-product':
            return tf.multiply(long_embed, short_embed)

        if self.config['long-short-interact'] == 'cross':
            cross_network = CrossNetwork()
            long_short_concat = tf.concat([long_embed, short_embed], axis=1)
            cross_output = cross_network.get_output(x0=long_short_concat, layer_num=self.config['cross_layer_num'],
                                     column_num=long_short_concat.get_shape().as_list()[1])

            output_fc_W = tf.get_variable(
                name="cross-output_fc_W",
                dtype=tf.float32,
                shape=[cross_output.get_shape().as_list()[1], self.numFactor],
                initializer=tf.contrib.layers.xavier_initializer()
            )

            output_fc_b = tf.get_variable(
                name="cross-output_fc_b",
                dtype=tf.float32,
                initializer=tf.constant(0.1, shape=[self.numFactor])
            )

            output = tf.nn.xw_plus_b(cross_output, output_fc_W, output_fc_b, name="scores")

            return output

        elif self.config['long-short-interact'] == 'outproduct-conv':
            outproduct_matrix = self.out_product(long_embed, short_embed)

            # conv_output = tf.reduce_sum(outproduct_matrix, axis=2)

            height = outproduct_matrix.get_shape().as_list()[1]
            width = outproduct_matrix.get_shape().as_list()[2]

            conv = CNN_Pool_Compoment(
                filter_num=self.config['out-product-filter-num'],
                filter_sizes=[i+1 for i in range(height)],
                wordvec_size=width,
                max_review_length=height,
                word_matrix=None,
                output_size=1,
                review_wordId_print=None,
                review_input_print=None,
                cnn_lambda=None,
                dropout_keep_prob=None,
                component_raw_output=None,
                item_pad_num=None,
                name='out-product-downstream'
            )

            conv_output = conv.get_horizontal_output_with_image(input_image=outproduct_matrix)

            conv_dim = conv_output.get_shape().as_list()[1]

            output_fc_W = tf.get_variable(
                name="outproduct-output_fc_W",
                dtype=tf.float32,
                shape=[conv_dim, self.numFactor],
                initializer=tf.contrib.layers.xavier_initializer()
            )

            output_fc_b = tf.get_variable(
                name="outproduct-output_fc_b",
                dtype=tf.float32,
                initializer=tf.constant(0.1, shape=[self.numFactor])
            )

            output = tf.nn.xw_plus_b(conv_output, output_fc_W, output_fc_b, name="scores")

            return output

        elif self.config['long-short-interact'] == 'self-attention':
            pass

        elif self.config['long-short-interact'] == 'concat-mlp':

            long_short_concat = tf.concat([long_embed, short_embed], axis=1)

            output_fc_W = tf.get_variable(
                name="outproduct-output_fc_W",
                dtype=tf.float32,
                shape=[long_short_concat.get_shape().as_list()[1], self.numFactor],
                initializer=tf.contrib.layers.xavier_initializer()
            )

            output_fc_b = tf.get_variable(
                name="outproduct-output_fc_b",
                dtype=tf.float32,
                initializer=tf.constant(0.1, shape=[self.numFactor])
            )

            output = tf.nn.xw_plus_b(long_short_concat, output_fc_W, output_fc_b, name="scores")

            return output


    def correlation(self, user_embed, item_embed, corr_type):
        if corr_type == 'dot-product':
            return self.dot_product(user_embed=user_embed, item_embed=item_embed)

        elif corr_type == 'cross':
            return self.cross_op(user_embed=user_embed, item_embed=item_embed, layer_num=self.config['corr-cross-layer-num'])

        elif corr_type == 'concat-mlp':
            return self.concat_mlp(user_embed=user_embed, item_embed=item_embed, layer_sizes=self.config['corr-mlp-layer-sizes'])

        elif corr_type == 'outproduct-conv':
            return self.out_product_conv(user_embed=user_embed, item_embed=item_embed)

    def correlations(self, user_embed, item_embeds, corr_type):

        return self.dot_products(user_embed=user_embed, item_embeds=item_embeds)

    def dot_products(self, user_embed, item_embeds):

        user_embed = tf.expand_dims(user_embed, axis=1)
        user_embed_broad = tf.tile(user_embed, [1, item_embeds.get_shape().as_list()[1], 1])

        dotproduct = tf.multiply(user_embed_broad, item_embeds)
        pred = tf.reduce_sum(dotproduct, 2, keep_dims=False)

        return pred

    def dot_product(self, user_embed, item_embed):

        dotproduct = tf.multiply(user_embed, item_embed)
        dotproduct = tf.reshape(dotproduct, shape=[-1, user_embed.get_shape().as_list()[1]])
        pred = tf.reduce_sum(dotproduct, 1, keep_dims=True)

        return pred

    def cross_op(self, user_embed, item_embed, layer_num):

        with tf.variable_scope('corr-cross', reuse=tf.AUTO_REUSE):
            if self.corr_network is None:
                self.corr_network = CrossNetwork()

            user_item_concat = tf.concat([user_embed, item_embed], axis=1)
            cross_output = self.corr_network.get_output(x0=user_item_concat, layer_num=layer_num,
                                            column_num=user_item_concat.get_shape().as_list()[1])

            output_fc_W = tf.get_variable(
                name="outproduct-output_fc_W",
                dtype=tf.float32,
                shape=[cross_output.get_shape().as_list()[1], 1],
                initializer=tf.contrib.layers.xavier_initializer(),
            )

            output_fc_b = tf.get_variable(
                name="outproduct-output_fc_b",
                dtype=tf.float32,
                initializer=tf.constant(0.1, shape=[1]),
            )

            output = tf.nn.xw_plus_b(cross_output, output_fc_W, output_fc_b, name="scores")

            return output

    def concat_mlp(self, user_embed, item_embed, layer_sizes):

        user_item_concat = tf.concat([user_embed, item_embed], axis=1)
        if self.corr_network is None:
            new_layer_sizes = [user_item_concat.get_shape().as_list()[1]] + layer_sizes

            self.corr_network = MLP(new_layer_sizes, dropout_keep=self.dropout_keep_placeholder, name='corr')
        output = self.corr_network.get_output(feature_input=user_item_concat)

        return output

    def out_product_conv(self, user_embed, item_embed):
        with tf.variable_scope('corr-out-product-conv', reuse=tf.AUTO_REUSE):
            outproduct_matrix = self.out_product(user_embed, item_embed)

            # conv_output = tf.reduce_sum(outproduct_matrix, axis=2)

            height = outproduct_matrix.get_shape().as_list()[1]
            width = outproduct_matrix.get_shape().as_list()[2]

            if self.corr_network is None:
                self.corr_network = CNN_Pool_Compoment(
                    filter_num=self.config['corr-out-product-filter-num'],
                    filter_sizes=[i + 1 for i in range(height)],
                    wordvec_size=width,
                    max_review_length=height,
                    word_matrix=None,
                    output_size=1,
                    review_wordId_print=None,
                    review_input_print=None,
                    cnn_lambda=None,
                    dropout_keep_prob=None,
                    component_raw_output=None,
                    item_pad_num=None,
                    name='corr'
                )

            conv_output = self.corr_network.get_horizontal_output_with_image(input_image=outproduct_matrix)

            conv_dim = conv_output.get_shape().as_list()[1]

            output_fc_W = tf.get_variable(
                name="outproduct-output_fc_W",
                dtype=tf.float32,
                shape=[conv_dim, self.item_fc_dim[-1] + self.numFactor],
                initializer=tf.contrib.layers.xavier_initializer()
            )

            output_fc_b = tf.get_variable(
                name="outproduct-output_fc_b",
                dtype=tf.float32,
                initializer=tf.constant(0.1, shape=[self.item_fc_dim[-1] + self.numFactor])
            )

            output = tf.nn.xw_plus_b(conv_output, output_fc_W, output_fc_b, name="scores")

            return tf.reduce_sum(output, axis=1, keepdims=True)

















