import numpy as np
import tensorflow as tf
import random
from src.recommender.BasicRcommender import BasicRecommender
import time
from src.component.Conv import CNN_Compoment
from src.component.Capsule import Capsule_Component
from src.component import MLP


class CapSeqMNRecommender(BasicRecommender):
    def __init__(self, dataModel, config):

        super(CapSeqMNRecommender, self).__init__(dataModel, config)

        self.train_users = dataModel.train_users
        self.train_sequences_input = dataModel.train_sequences_input
        self.train_sequences_target = dataModel.train_sequences_target
        self.user_pred_sequences = dataModel.user_pred_sequences
        self.batch_end_users = dataModel.batch_end_users

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.name = 'CapSeq_MN'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.target_length = config['target_length']
        self.filter_num = config['filter_num']
        self.dropout_keep = config['dropout_keep']
        self.capsule_num = config['capsule_num']
        self.dynamic_routing_iter = config['dynamic_routing_iter']
        self.loss = config['loss']
        self.feature_num = config['feature_num']

        self.clear_user = tf.placeholder_with_default(False, shape=())
        self.user_clear_list = tf.placeholder(tf.int32, [None])

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])
        self.input_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.input_length])
        self.target_seq_pos = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [self.trainBatchSize, 100])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())
        self.is_training = tf.placeholder_with_default(False, shape=())

        # user/item embedding
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        self.itemBias = tf.Variable(tf.random_normal([self.numItem], 0, 0.1))

        self.feature_key = tf.Variable(tf.random_normal([self.feature_num, self.numFactor], 0, 0.1))
        self.memory = tf.Variable(tf.random_normal([self.numUser, self.feature_num, self.numFactor], 0, 0.1))

        # erase
        self.earse_W = tf.Variable(tf.random_normal([self.numFactor, self.numFactor], 0, 0.1))
        self.earse_b = tf.Variable(tf.random_normal([self.numFactor], 0, 0.1))

        # item Conv-Capsule
        self.item_convs = []
        self.item_pattern_capsules = []

        for i in range(self.input_length):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='item'
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.filter_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.input_length - filter_height + 1,
                out_vec_len=self.input_length - filter_height + 1,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='item'
            )
            self.item_convs.append(conv)
            self.item_pattern_capsules.append(capsule)

        # time capsule
        self.item_time_capsule = Capsule_Component(
            bs=self.trainBatchSize,
            num_caps_i=self.input_length,
            num_caps_j=self.capsule_num,
            in_vec_len=self.numFactor,
            out_vec_len=self.numFactor,
            user_vec_len=self.numFactor,
            user_bias=True,
            T=self.dynamic_routing_iter,
            name='item_time'
        )

        self.item_concate_size = (((
                                   self.input_length + 1) * self.input_length) / 2 + self.numFactor) * self.capsule_num + self.numFactor

        self.item_mlp = MLP([self.item_concate_size] + config['item_fc_dim'],
                            dropout_keep=self.dropout_keep_placeholder)


    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            item_input_image = tf.nn.embedding_lookup(self.itemEmbedding, self.input_seq)
            item_input_image = tf.reshape(item_input_image, [-1, self.input_length, self.numFactor])
            item_input_image = tf.expand_dims(item_input_image, -1)

            neg_preds = self.get_pred(item_input_image, self.target_seq_neg, self.target_length)
            pos_preds = self.get_pred(item_input_image, self.target_seq_pos, self.target_length)

            if self.loss == 'bpr':
                rating_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            else:
                rating_loss = - tf.reduce_mean(tf.log(tf.nn.sigmoid(pos_preds))) - tf.reduce_mean(tf.log(1 - tf.nn.sigmoid(neg_preds)))

            self.cost = rating_loss

            self.r_pred = self.get_pred(item_input_image, self.pred_seq, 100)

            self.write_memory(self.u_id, self.target_seq_pos)

            # clear the memroy
            zeros_dims = tf.stack([tf.shape(self.user_clear_list)[0], self.feature_num, self.numFactor])
            zeros = tf.fill(zeros_dims, 0.0)
            self.memory = tf.scatter_update(self.memory, self.user_clear_list, zeros)

    def get_pred(self, image_input, target_item_ids, target_length):

        split_list = [1] * target_length
        target_item_id_list = tf.split(target_item_ids, split_list, 1)
        preds = []

        for target_item_id in target_item_id_list:
            target_itemEmbedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, target_item_id),
                                           [-1, self.numFactor])
            # [bs, self.numFactor]

            memory_out = self.read_memory(self.u_id, target_itemEmbedding)

            item_embeddings = self.encode_image(image_input, self.input_length, self.item_convs, self.item_pattern_capsules, self.item_time_capsule, memory_out)

            item_embeddings.append(memory_out)
            fusion_vector = tf.concat(values=item_embeddings, axis=1)
            merged_embed = self.item_mlp.get_output(feature_input=fusion_vector)

            element_wise_mul = tf.multiply(merged_embed, target_itemEmbedding)
            element_wise_mul_drop = tf.nn.dropout(element_wise_mul, self.dropout_keep_placeholder)

            log_intention = tf.reshape(tf.reduce_sum(element_wise_mul_drop, axis=1), [-1, 1])

            preds.append(log_intention)

        return tf.concat(preds, axis=1)

    def read_memory(self, user_id, current_item_embedding):
        # current_item_embedding = [bs, numFactor]

        memory_batch_read = tf.reshape(tf.nn.embedding_lookup(self.memory, user_id),
                                       [-1, self.feature_num, self.numFactor])
        # [bs, feature_num, numFactor]

        batch_key = tf.expand_dims(self.feature_key, axis=0)
        # [1, feature_num, numFactor]

        batch_key_tailed = tf.tile(batch_key, [self.trainBatchSize, 1, 1])
        # [bs, feature_num, numFactor]

        expanded_item_embedding = tf.expand_dims(current_item_embedding, axis=1)
        # [bs, 1, numFactor]

        dot_product = tf.multiply(batch_key_tailed, expanded_item_embedding)
        # [bs, feature_num, numFactor]

        score = tf.reduce_sum(dot_product, axis=2)
        # [bs, feature_num]

        weight = tf.nn.softmax(score)
        # [bs, feature_num]

        self.weight = tf.expand_dims(weight, axis=2)
        # [bs, feature_num, 1]

        out = tf.reduce_sum(tf.multiply(memory_batch_read, self.weight), axis=1)
        # [bs, numFactor]

        return out

    def erase(self, i):
        out = tf.nn.sigmoid(i)
        return out

    def add(self, i):
        out = tf.nn.tanh(i)
        return out

    def write_memory(self, user_id, target_item_id):
        '''

        :param user_id: [bs, 1]
        :param item_ids: [bs, target_len]
        :param len:
        :return:
        '''

        current_item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, target_item_id),
                                            [-1, self.numFactor])
        # [bs, numFactor]
        memory_batch_write = tf.reshape(tf.nn.embedding_lookup(self.memory, user_id),
                                        [-1, self.feature_num, self.numFactor])
        # [bs, feature_num, numFactor]
        ones = tf.ones([1, self.feature_num, self.numFactor], tf.float32)
        # [1, feature_num, numFactor]
        e = tf.expand_dims(self.erase(current_item_embedding), axis=1)
        # [bs, 1, numFactor]
        e = tf.tile(e, [1, self.feature_num, 1])
        # [bs, feature_num, numFactor]
        a = tf.expand_dims(self.add(current_item_embedding), axis=1)
        # [bs, 1, numFactor]
        a = tf.tile(a, [1, self.feature_num, 1])
        # [bs, feature_num, numFactor]
        decay = tf.subtract(ones, tf.multiply(self.weight, e))
        # [bs, feature_num, numFactor]
        increase = tf.multiply(self.weight, a)
        # [bs, feature_num, numFactor]
        new_value = tf.multiply(memory_batch_write, decay) + increase
        # [bs, feature_num, numFactor]

        user_id = tf.reshape(user_id, [-1])
        self.memory = tf.scatter_update(self.memory, user_id, new_value)

    def encode_image(self, input_image, image_height, convs, hor_capsules, ver_capsules, comple_embed):
        embeddings = []
        for i in range(image_height):
            filter_height = i + 1
            conv = convs[i].get_output(input_image)
            # assert conv.get_shape() == [-1, self.input_length-filter_height+1, 1, self.filter_num]

            conv = tf.transpose(conv, perm=[0, 3, 1, 2])
            # assert conv.get_shape() == [-1, self.filter_num, self.input_length-filter_height+1, 1]

            # assert conv.get_shape() == [-1, self.input_length - filter_height + 1, self.filter_num, 1]
            # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
            output = hor_capsules[i].get_output(conv, comple_embed)
            # assert output.get_shape() == [-1, self.input_length - filter_height + 1]
            output = tf.reshape(output, [-1, self.capsule_num * (self.input_length - filter_height + 1)])
            embeddings.append(output)

        time_output = ver_capsules.get_output(input_image, comple_embed)
        time_output = tf.reshape(time_output, [-1, self.capsule_num * self.numFactor])
        # assert time_output.get_shape() == [-1, capsule_num * self.numFactor]
        embeddings.append(time_output)

        return embeddings

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict_train, feed_dict_memory = self.getTrainData(batchId)

        self.optimizer.run(feed_dict=feed_dict_train)
        self.sess.run([self.memory],
                      feed_dict=feed_dict_memory)
        loss = self.cost.eval(feed_dict=feed_dict_train)

        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        neg_seq_batch = []

        start_idx = batchId * self.trainBatchSize

        if start_idx + self.trainBatchSize >= self.trainSize:
            remainder = start_idx + self.trainBatchSize - self.trainSize
            user_batch = self.train_users[start_idx:] + self.train_users[:remainder]
            input_seq_batch = self.train_sequences_input[start_idx:] + self.train_sequences_input[:remainder]
            pos_seq_batch = self.train_sequences_target[start_idx:] + self.train_sequences_target[:remainder]

        else:
            end_idx = start_idx + self.trainBatchSize
            user_batch = self.train_users[start_idx:end_idx]
            input_seq_batch = self.train_sequences_input[start_idx:end_idx]
            pos_seq_batch = self.train_sequences_target[start_idx:end_idx]

        for userIdx in user_batch:
            neg_items = []
            for i in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_seq_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((self.trainBatchSize, 1))
        input_seq_batch = np.array(input_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_seq_batch)
        user_clear_list = self.batch_end_users[batchId]

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict_train = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep,
            self.pred_seq: np.repeat(user_batch, 100, axis=1),
        }

        feed_dict_memory = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.pred_seq: np.repeat(user_batch, 100, axis=1),
            self.user_clear_list: user_clear_list
        }

        return feed_dict_train, feed_dict_memory

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_seq = []
        target_seq = []

        for userIdx in user_idices:
            input_seq.append(self.user_pred_sequences[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        input_seq = np.array(input_seq)
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.input_seq: input_seq,
            self.pred_seq: target_seq,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(end - start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
