import numpy as np
import tensorflow as tf
import random
from src.recommender.BasicRcommender import BasicRecommender
import time
from src.component.Conv import CNN_Compoment
from src.component.Capsule import Capsule_Component
from src.component import MLP

class CapSeqPRecommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(CapSeqPRecommender, self).__init__(dataModel, config)

        self.train_users = dataModel.train_users
        self.train_sequences_input = dataModel.train_sequences_input
        self.train_sequences_target = dataModel.train_sequences_target
        self.user_pred_sequences = dataModel.user_pred_sequences

        self.trainSize = len(self.train_users)
        self.trainBatchNum = int(self.trainSize // self.trainBatchSize) + 1

        self.name = 'CapSeq_MN-plus'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.input_length = config['input_length']
        self.target_length = config['target_length']
        self.filter_num = config['filter_num']
        self.dropout_keep = config['dropout_keep']
        self.capsule_num = config['capsule_num']
        self.dynamic_routing_iter = config['dynamic_routing_iter']
        self.feature_vec_num = config['feature_vec_num']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [self.trainBatchSize, 1])
        self.input_seq = tf.placeholder(tf.int32, [self.trainBatchSize, self.input_length])
        self.target_seq_pos = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.target_seq_neg = tf.placeholder(tf.int32, [self.trainBatchSize, self.target_length])
        self.pred_seq = tf.placeholder(tf.int32, [self.trainBatchSize, 100])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())
        self.is_training = tf.placeholder_with_default(False, shape=())

        # user/item embedding
        self.userEmbedding = tf.Variable(tf.random_normal([self.numUser, self.numFactor], 0, 0.1))
        self.itemEmbedding = tf.Variable(tf.random_normal([self.numItem, self.numFactor], 0, 0.1))
        self.itemBias = tf.Variable(tf.random_normal([self.numItem], 0, 0.1))

        self.memory_key = tf.Variable(tf.random_normal([self.feature_vec_num, self.numFactor], 0, 0.1))
        self.user_memory = tf.Variable(tf.random_normal([self.numUser, self.feature_vec_num, self.numFactor], 0, 0.1))

        # erase
        self.earse_W = tf.Variable(tf.random_normal([self.numFactor, self.numFactor], 0, 0.1))
        self.earse_b = tf.Variable(tf.random_normal([self.numFactor], 0, 0.1))

        # item Conv-Capsule
        self.item_convs = []
        self.item_pattern_capsules = []

        for i in range(self.input_length):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='item'
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.filter_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.input_length - filter_height + 1,
                out_vec_len=self.input_length - filter_height + 1,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='item'
            )
            self.item_convs.append(conv)
            self.item_pattern_capsules.append(capsule)

        # memory network Conv-Capsule
        self.feature_convs = []
        self.feature_pattern_capsules = []
        for i in range(self.feature_vec_num):
            filter_height = i + 1
            conv = CNN_Compoment(
                filter_num=self.filter_num,
                filter_height=filter_height,
                filter_width=self.numFactor,
                name='feature'
            )
            capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.filter_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.input_length - filter_height + 1,
                out_vec_len=self.input_length - filter_height + 1,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='feature'
            )
            self.feature_convs.append(conv)
            self.feature_pattern_capsules.append(capsule)


        # time capsule
        self.item_time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='item_time'
            )
        # time capsule
        self.feature_time_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.feature_vec_num,
                num_caps_j=self.capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=True,
                T=self.dynamic_routing_iter,
                name='feature_time'
            )

        self.item_concate_size = (((self.input_length + 1) * self.input_length) / 2 + self.numFactor) * self.capsule_num + self.numFactor
        self.feature_concate_size = (((self.feature_vec_num + 1) * self.input_length) / 2 + self.numFactor) * self.capsule_num + self.numFactor
        self.item_mlp = MLP([self.item_concate_size] + config['item_fc_dim'], dropout_keep=self.dropout_keep_placeholder)
        self.feature_mlp = MLP([self.feature_concate_size] + config['item_fc_dim'], dropout_keep=self.dropout_keep_placeholder)

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            # --------------- read -------------

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])

            item_input_image = tf.nn.embedding_lookup(self.itemEmbedding, self.input_seq)
            item_input_image = tf.reshape(item_input_image, [-1, self.input_length, self.numFactor])
            item_input_image = tf.expand_dims(item_input_image, -1)

            userMemory = tf.reshape(tf.nn.embedding_lookup(self.user_memory, self.u_id), [-1, self.input_length, self.numFactor])
            memory_input_image = tf.expand_dims(userMemory, -1)

            # assert input_image.get_shape() == [-1, self.input_length, self.numFactor, 1]
            feature_embeddings = self.encode_image(input_image=memory_input_image,
                                                image_height=self.feature_vec_num,
                                                convs=self.feature_convs,
                                                hor_capsules=self.feature_pattern_capsules,
                                                ver_capsules=self.feature_time_capsule,
                                                comple_embed=None)

            feature_concate_vec = tf.concat(values=feature_embeddings, axis=1)
            feature_merge = self.feature_mlp.get_output(feature_input=feature_concate_vec)

            item_embeddings = self.encode_image(input_image=item_input_image,
                                                image_height=self.input_length,
                                                convs=self.item_convs,
                                                hor_capsules=self.item_pattern_capsules,
                                                ver_capsules=self.item_time_capsule,
                                                comple_embed=feature_merge)

            item_embeddings.append(feature_merge)
            fusion_vector = tf.concat(values=item_embeddings, axis=1)
            merged_embed = self.item_mlp.get_output(feature_input=fusion_vector)
            # assert merged_embed.get_shape() == [-1, self.numFactor]

            # --------------- write -------------




            # --------------- predict -------------

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )

    def read_memory(self, user_id, current_item_embedding):
        # current_item_embedding = [bs, numFactor]

        memory_batch_read = tf.reshape(tf.nn.embedding_lookup(self.memory, user_id), [-1, self.feature_num, self.numFactor])
        # [bs, feature_num, numFactor]

        batch_key = tf.expand_dims(self.feature_key, axis=0)
        # [1, feature_num, numFactor]

        batch_key_tailed = tf.tile(batch_key, [self.trainBatchSize, 1, 1])
        # [bs, feature_num, numFactor]

        expanded_item_embedding = tf.expand_dims(current_item_embedding, axis=1)
        # [bs, 1, numFactor]

        dot_product = tf.multiply(batch_key_tailed, expanded_item_embedding)
        # [bs, feature_num, numFactor]

        score = tf.reduce_sum(dot_product, axis=2)
        # [bs, feature_num]

        weight = tf.nn.softmax(score)
        # [bs, feature_num]

        self.weight = tf.expand_dims(weight, axis=2)
        # [bs, feature_num, 1]

        out = tf.reduce_sum(tf.multiply(memory_batch_read, self.weight), axis=1)
        # [bs, numFactor]

        return out

    def erase(self, i):
        out = tf.nn.sigmoid(i)
        return out

    def add(self, i):
        out = tf.nn.tanh(i)
        return out

    def write_memory(self, user_id, target_item_id):
        '''

        :param user_id: [bs, 1]
        :param item_ids: [bs, target_len]
        :param len:
        :return:
        '''

        current_item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, target_item_id),
                                            [-1, self.numFactor])
        # [bs, numFactor]
        memory_batch_write = tf.reshape(tf.nn.embedding_lookup(self.memory, user_id),
                                        [-1, self.feature_num, self.numFactor])
        # [bs, feature_num, numFactor]
        ones = tf.ones([1, self.feature_num, self.numFactor], tf.float32)
        # [1, feature_num, numFactor]
        e = tf.expand_dims(self.erase(current_item_embedding), axis=1)
        # [bs, 1, numFactor]
        e = tf.tile(e, [1, self.feature_num, 1])
        # [bs, feature_num, numFactor]
        a = tf.expand_dims(self.add(current_item_embedding), axis=1)
        # [bs, 1, numFactor]
        a = tf.tile(a, [1, self.feature_num, 1])
        # [bs, feature_num, numFactor]
        decay = tf.subtract(ones, tf.multiply(self.weight, e))
        # [bs, feature_num, numFactor]
        increase = tf.multiply(self.weight, a)
        # [bs, feature_num, numFactor]
        new_value = tf.multiply(memory_batch_write, decay) + increase
        # [bs, feature_num, numFactor]

        user_id = tf.reshape(user_id, [-1])
        self.memory = tf.scatter_update(self.memory, user_id, new_value)



    def encode_image(self, input_image, image_height, convs, hor_capsules, ver_capsules, comple_embed):
        embeddings = []
        for i in range(image_height):
            filter_height = i + 1
            conv = convs[i].get_output(input_image)
            # assert conv.get_shape() == [-1, self.input_length-filter_height+1, 1, self.filter_num]

            conv = tf.transpose(conv, perm=[0, 3, 1, 2])
            # assert conv.get_shape() == [-1, self.filter_num, self.input_length-filter_height+1, 1]

            # assert conv.get_shape() == [-1, self.input_length - filter_height + 1, self.filter_num, 1]
            # raw capsule output shape = [batch_size, num_j_capsule, output_vec_len, 1]
            output = hor_capsules[i].get_output(conv, comple_embed)
            # assert output.get_shape() == [-1, self.input_length - filter_height + 1]
            output = tf.reshape(output, [-1, self.capsule_num * (self.input_length - filter_height + 1)])
            embeddings.append(output)

        time_output = ver_capsules.get_output(input_image, comple_embed)
        time_output = tf.reshape(time_output, [-1, self.capsule_num * self.numFactor])
        # assert time_output.get_shape() == [-1, capsule_num * self.numFactor]
        embeddings.append(time_output)

        return embeddings

    def get_pred(self, merged_embed, ids):
        pred_embeds = tf.transpose(tf.nn.embedding_lookup(self.itemEmbedding, ids), perm=[1, 0, 2])
        pred_bias = tf.transpose(tf.nn.embedding_lookup(self.itemBias, ids))
        pred = tf.reduce_sum(merged_embed*pred_embeds, 1, keep_dims=True) + pred_bias
        return tf.transpose(pred)

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        feed_dict = self.getTrainData(batchId)

        self.optimizer.run(feed_dict=feed_dict)
        loss = self.cost.eval(feed_dict=feed_dict)

        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        neg_seq_batch = []

        start_idx = batchId * self.trainBatchSize
        end_idx = start_idx + self.trainBatchSize

        if end_idx > self.trainSize:
            end_idx = self.trainSize
            start_idx = end_idx - self.trainBatchSize

        if end_idx == start_idx:
            start_idx = 0
            end_idx = start_idx + self.trainBatchSize

        user_batch = self.train_users[start_idx:end_idx]
        input_seq_batch = self.train_sequences_input[start_idx:end_idx]
        pos_seq_batch = self.train_sequences_target[start_idx:end_idx]

        for userIdx in user_batch:
            neg_items = []
            for i in range(self.target_length):
                positiveItems = self.user_items_train[userIdx]
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)
            neg_seq_batch.append(neg_items)

        user_batch = np.array(user_batch).reshape((end_idx - start_idx, 1))
        input_seq_batch = np.array(input_seq_batch)
        pos_seq_batch = np.array(pos_seq_batch)
        neg_seq_batch = np.array(neg_seq_batch)

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict = {
            self.u_id: user_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep
        }

        return feed_dict

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        input_seq = []
        target_seq = []

        for userIdx in user_idices:
            input_seq.append(self.user_pred_sequences[userIdx])
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        input_seq = np.array(input_seq)
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.input_seq: input_seq,
            self.pred_seq: target_seq,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * 100
            end = start + 100
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
