
from src.recommender.GRU4Rec import GRU4RecRecommender
from src.data_model.SequenceDataModel import SequenceDataModel

if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 939,
        'testBatchSize': 939,
        'numFactor': 32,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'seq_length': 5,
        'dropout_keep': 0.5,
        'rnn_unit_num': 64,
        'rnn_layer_num': 1,
        'rnn_cell': 'GRU',
        'eval_item_num': 100,
        'seq_direc': 'hor',
        'early_stop': True,
        'random_seed': 1,
        'save_model': False
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName
        seq_length = config['seq_length']

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for seq_length in [5]:
            config['seq_length'] = seq_length
            recommender = GRU4RecRecommender(dataModel, config)
            recommender.run()

        # for rnn_unit_num in [32, 64, 128, 256]:
        #     for rnn_layer_num in [1]:
        #         for dp_kp in [1.0, 0.9, 0.8, 0.7, 0.6, 0.5]:
        #             config['dropout_keep'] = dp_kp
        #             config['rnn_unit_num'] = rnn_unit_num
        #             config['rnn_layer_num'] = rnn_layer_num
























