
from recommender.Caser_RNN import Caser_RNN_Recommender
from src.data_model.SequenceDataModel import SequenceDataModel

if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 512,
        'testBatchSize': 1000,
        'numFactor': 32,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 5,
        'target_length': 3,
        'hor_filter_num': 16,
        'ver_filter_num': 1,
        'dropout_keep': 0.5,
        'item_fc_dim': [32],
        'rnn_unit_num': 64,
        'rnn_layer_num': 1,
        'rnn_cell': 'GRU',
        'eval_item_num': 100,
        'if_local_position': True,
        'if_global_position': True,
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for input_length in [1, 2, 3, 4, 5, 6, 7, 8, 9]:
            for target_length in [1]:
                for drop_keep in [0.8, 0.7]:

                    config['input_length'] = input_length
                    config['target_length'] = target_length
                    config['dropout_keep'] = drop_keep

                    dataModel.generate_sequences_hor(input_length, target_length)

                    recommender = Caser_RNN_Recommender(dataModel, config)
                    recommender.run()

        config['if_global_position'] = False
        for input_length in [1, 2, 3, 4, 5, 6, 7, 8, 9]:
            for target_length in [1]:
                for drop_keep in [0.8, 0.7]:

                    config['input_length'] = input_length
                    config['target_length'] = target_length
                    config['dropout_keep'] = drop_keep

                    dataModel.generate_sequences_hor(input_length, target_length)

                    recommender = Caser_RNN_Recommender(dataModel, config)
                    recommender.run()
















