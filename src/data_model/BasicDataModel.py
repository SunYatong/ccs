import random
import logging
import os.path
import time
from operator import itemgetter, attrgetter, methodcaller
import tensorflow as tf
import numpy as np

class BasicDataModel:

    def __init__(self, config):
        self.fileName = config['fileName']
        self.inputPath = './dataset/processed_datasets/' + self.fileName
        self.outputPath = '../dataset/processed_datasets/' + self.fileName
        self.eval_item_num = config['eval_item_num']
        self.logger = self.initialize_logger('./log', config)
        self.trainSet = []
        self.validSet = []
        self.testSet = []
        self.wholeDataSet = []
        self.trainSize = 0
        self.validSize = 0
        self.testSize = 0
        self.numUser = 0
        self.numItem = 0
        self.numWord = 0
        self.userIdSet = set()
        self.itemIdSet = set()
        self.user_items_train = {}
        self.user_items_train_paded = {}
        self.user_items_test = {}
        self.user_rating_scale = {}

        self.testMatrix = {}
        self.trainType = config['trainType']
        self.splitterType = config['splitterType']


        self.ratingScaleSet = set()
        self.userSet = set()
        self.itemSet = set()

        self.userIdToUserIdx = {}
        self.itemIdToItemIdx = {}
        self.userIdxToUserId = {}
        self.itemIdxToItemId = {}
        self.itemsInTestSet = set()
        self.evalItemsForEachUser = {}
        self.contains_time = False
        if config['goal'] == 'rating:':
            self.threshold = -1
        else:
            self.threshold = config['threshold']

        random.seed(123)

    def readData(self):
        trainPath = self.inputPath + '/' + self.splitterType + '/train.txt'
        validPath = self.inputPath + '/' + self.splitterType + '/valid.txt'
        testPath = self.inputPath + '/' + self.splitterType + '/test.txt'
        # read train data
        basicUserIdx = 0
        basicItemIdx = 0
        user_in_testSet = set()
        with open(trainPath) as trainFile:
            for line in trainFile:
                record = line.split(' ')
                userId, itemId, rating = record[0], record[1], float(record[2])

                if userId not in self.userIdToUserIdx:
                    userIdx = basicUserIdx
                    self.userIdToUserIdx[userId] = userIdx
                    self.userIdxToUserId[userIdx] = userId
                    basicUserIdx += 1
                if itemId not in self.itemIdToItemIdx:
                    itemIdx = basicItemIdx
                    self.itemIdToItemIdx[itemId] = itemIdx
                    self.itemIdxToItemId[itemIdx] = itemId
                    basicItemIdx += 1

                userIdx = self.userIdToUserIdx[userId]
                itemIdx = self.itemIdToItemIdx[itemId]

                if self.threshold < 0:
                    pass
                elif rating > self.threshold:
                    rating = 1.0
                else:
                    rating = 0.0

                self.user_items_train.setdefault(userIdx, [])
                self.user_items_train[userIdx].append(itemIdx)

                self.ratingScaleSet.add(rating)
                self.trainSet.append([userIdx, itemIdx, rating])

        # read valid data
        with open(validPath) as validFile:
            for line in validFile:
                record = line.split(' ')
                userId, itemId, rating = record[0], record[1], float(record[2])

                if userId not in self.userIdToUserIdx:
                    userIdx = basicUserIdx
                    self.userIdToUserIdx[userId] = userIdx
                    self.userIdxToUserId[userIdx] = userId
                    basicUserIdx += 1
                if itemId not in self.itemIdToItemIdx:
                    itemIdx = basicItemIdx
                    self.itemIdToItemIdx[itemId] = itemIdx
                    self.itemIdxToItemId[itemIdx] = itemId
                    basicItemIdx += 1

                userIdx = self.userIdToUserIdx[userId]
                itemIdx = self.itemIdToItemIdx[itemId]

                if self.threshold < 0:
                    pass
                elif rating > self.threshold:
                    rating = 1.0
                else:
                    rating = 0.0

                self.user_items_train.setdefault(userIdx, [])
                self.user_items_train[userIdx].append(itemIdx)

                self.ratingScaleSet.add(rating)
                self.validSet.append([userIdx, itemIdx, rating])

        # read test data
        with open(testPath) as testFile:
            for line in testFile:
                record = line.split(' ')
                userId, itemId, rating = record[0], record[1], float(record[2])

                if userId not in self.userIdToUserIdx:
                    userIdx = basicUserIdx
                    self.userIdToUserIdx[userId] = userIdx
                    self.userIdxToUserId[userIdx] = userId
                    basicUserIdx += 1
                if itemId not in self.itemIdToItemIdx:
                    itemIdx = basicItemIdx
                    self.itemIdToItemIdx[itemId] = itemIdx
                    self.itemIdxToItemId[itemIdx] = itemId
                    basicItemIdx += 1

                userIdx = self.userIdToUserIdx[userId]
                itemIdx = self.itemIdToItemIdx[itemId]
                user_in_testSet.add(userIdx)

                # no binarize for testset
                # if self.threshold < 0:
                #     pass
                # elif rating > self.threshold:
                #     rating = 1.0
                # else:
                #     rating = 0.0
                if userIdx not in self.user_items_test.keys():
                    self.user_items_test[userIdx] = []
                self.user_items_test[userIdx].append(itemIdx)
                self.testSet.append([int(userIdx), int(itemIdx), float(rating)])
                self.itemsInTestSet.add(itemIdx)

                if userIdx not in self.evalItemsForEachUser:
                    self.evalItemsForEachUser[userIdx] = set()

        if self.trainType == 'test':
            self.trainSet = self.trainSet + self.validSet
        else:
            self.testSet = self.validSet

        self.logger.info('Num user in testSet: ' + str(len(user_in_testSet)))

        self.trainSize = len(self.trainSet)
        self.testSize = len(self.testSet)
        self.numUser = len(self.userIdToUserIdx)
        self.numItem = len(self.itemIdToItemIdx)

        # print userIdx to userId
        userIdx_id_output_path = self.inputPath + '/' + self.splitterType + '/user_idx_id.txt'
        userIdx_id_output_outputLines = []
        userIdx_id_output_outputLines.append("idx   id\n")
        for userId in self.userIdToUserIdx:
            userIdx = self.userIdToUserIdx[userId]
            userIdx_id_output_outputLines.append(str(userIdx) + "   " + str(userId) + "\n")
        with open(userIdx_id_output_path, 'w') as userIdx_id_output_file:
            userIdx_id_output_file.writelines(userIdx_id_output_outputLines)

        # print itemIdx to itemId
        itemIdx_id_output_path = self.inputPath + '/' + self.splitterType + '/item_idx_id.txt'
        itemIdx_id_outputLines = []
        itemIdx_id_outputLines.append("idx  id\n")
        for itemId in self.itemIdToItemIdx:
            itemIdx = self.itemIdToItemIdx[itemId]
            itemIdx_id_outputLines.append(str(itemIdx) + "  " + str(itemId + "\n"))
        with open(itemIdx_id_output_path, 'w') as itemIdx_id_output_file:
            itemIdx_id_output_file.writelines(itemIdx_id_outputLines)



    def generateEvalItemsForEachUser(self):

        for userIdx in self.evalItemsForEachUser:
            itemsToEval = set(self.user_items_test[userIdx])
            itemsInTrain = self.user_items_train[userIdx]
            while len(itemsToEval) < self.eval_item_num:
                newItemIdx = random.randint(0, self.numItem-1)
                if newItemIdx not in itemsInTrain:
                    itemsToEval.add(newItemIdx)
            self.evalItemsForEachUser[userIdx] = list(itemsToEval)

    def printInfo(self):
        self.logger.info('dataset: ' + str(self.fileName))
        self.logger.info('trainType: ' + str(self.trainType))
        self.logger.info('trainSize: ' + str(self.trainSize))
        self.logger.info('testSize: ' + str(self.testSize))
        self.logger.info('numUser: ' + str(self.numUser))
        self.logger.info('numItem: ' + str(self.numItem))
        self.logger.info('numRating:' + str(self.trainSize + self.testSize))
        self.logger.info('ratingScale: ' + str(self.ratingScaleSet))
        self.logger.info('density: ' + str((self.trainSize + self.testSize) / (self.numItem * self.numUser)))
        self.logger.info('Num item in testSet: ' + str(len(self.itemsInTestSet)))

    def initialize_logger(self, output_dir, conf):

        logger = logging.getLogger(name='hhh')

        logger.setLevel(logging.DEBUG)

        # create console handler and set level to info
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter("%(levelname)s - %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        # create error file handler and set level to error
        handler = logging.FileHandler(os.path.join(output_dir, "error.log"), "w", encoding=None, delay="true")
        handler.setLevel(logging.ERROR)
        formatter = logging.Formatter("%(levelname)s - %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        # create debug file handler and set level to debug
        log_path=os.path.join(output_dir, conf['test-info'] + str(time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))) + '.log')
        # print('log path: ' + log_path)
        handler = logging.FileHandler(log_path, "w")
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(levelname)s - %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        return logger

    def buildTestMatrix(self):
        for line in self.testSet:
            userIdx, itemIdx, rating = line
            self.testMatrix[userIdx, itemIdx] = rating
        return self.testMatrix


    def listToString(self, list):
        listString = ''
        for i in range(len(list)):
            if i == len(list)-1:
                listString = listString + str(list[i])
            else:
                listString = listString + str(list[i]) + ':'

        return  listString



    def pre_process(self):

        f = open(self.inputPath + '/' + self.splitterType + '/full.txt')

        lines = f.readlines()
        # check if there is time column
        first_line = lines[0]

        if len(first_line.strip().split(' ')) == 4:
            self.contains_time = True

        for line in lines:
            record = line.strip().split(' ')

            userId, itemId, rating = record[0], record[1], record[2]

            self.userIdSet.add(userId)
            self.itemIdSet.add(itemId)

            if self.contains_time:
                self.wholeDataSet.append([userId, itemId, rating, record[3]])
            else:
                self.wholeDataSet.append([userId, itemId, rating])

        self.numRatingBeforeSplit = len(self.wholeDataSet)

        self.logger.info('raw_numUser:' + str(len(self.userIdSet)))
        self.logger.info('raw_numItem:' + str(len(self.itemIdSet)))
        self.logger.info('raw_numRating:' + str(self.numRatingBeforeSplit))

    def split_user_loo(self):
        random.seed(123)
        user_items = {}
        userItemToRating = {}

        # collect all data
        for record in self.wholeDataSet:
            userId, itemId, rating = record[0], record[1], record[2]
            user_items.setdefault(userId, [])
            user_items[userId].append(itemId)
            userItemToRating[userId, itemId] = rating

        # split the data
        trainSet = []
        validSet = []
        testSet = []
        itemsInTrainSet = set()
        usersInTrainSet = set()

        for userId in user_items:
            items = user_items[userId]
            random.shuffle(items)
            numItem = len(items)

            if numItem <= 2:
                for itemId in items:
                    rating = userItemToRating[userId, itemId]
                    trainSet.append([userId, itemId, rating])
            else:
                for itemId in items[: -2]:
                    rating = userItemToRating[userId, itemId]
                    trainSet.append([userId, itemId, rating])
                    itemsInTrainSet.add(itemId)
                    usersInTrainSet.add(userId)
                for itemId in items[-2: -1]:
                    rating = userItemToRating[userId, itemId]
                    validSet.append([userId, itemId, rating])
                for itemId in items[-1:]:
                    rating = userItemToRating[userId, itemId]
                    testSet.append([userId, itemId, rating])

        self.write_split_data(trainSet, validSet, testSet, usersInTrainSet, itemsInTrainSet)

    def split_UserTimeLOO(self, K):

        random.seed(123)
        # collect all user-items mappings
        user_items = {}
        # collect all user item - rating review timeStamp mappings
        userItemToRatingTimeStamp = {}
        for record in self.wholeDataSet:
            userId, itemId, rating, timeStamp = record
            timeStamp = float(timeStamp)
            user_items.setdefault(userId, [])
            user_items[userId].append([itemId, timeStamp])
            userItemToRatingTimeStamp[userId, itemId] = [rating, timeStamp]

        # split the data
        trainSet = []
        validSet = []
        testSet = []
        itemsInTrainSet = set()
        usersInTrainSet = set()
        for userId in user_items:
            # get and sort items
            items = sorted(user_items[userId], key=itemgetter(1))
            # split first n items into train set, except the last 2
            # items for trainSet
            for itemId, timeStamp in items[: -K]:
                rating, timeStamp = userItemToRatingTimeStamp[userId, itemId]
                trainSet.append([userId, itemId, rating, timeStamp])
                itemsInTrainSet.add(itemId)
                usersInTrainSet.add(userId)

            # items for validSet
            for itemId, timeStamp in items[-K:-K+1]:
                rating, timeStamp = userItemToRatingTimeStamp[userId, itemId]
                validSet.append([userId, itemId, rating, timeStamp])

            # items for testSet
            for itemId, timeStamp in items[-K+1:]:
                rating, timeStamp = userItemToRatingTimeStamp[userId, itemId]
                testSet.append([userId, itemId, rating, timeStamp])

        self.write_split_data(trainSet, validSet, testSet, usersInTrainSet, itemsInTrainSet)


    def write_split_data(self, trainSet, validSet, testSet, usersInTrainSet, itemsInTrainSet):

        trainSetToWrite = []
        validSetToWrite = []
        testSetToWrite = []

        for record in trainSet:

            userId, itemId, rating = record[0], record[1], record[2]
            trainSetToWrite.append(
                str(userId) + " " + str(itemId) + " " + str(rating) + '\n')

        for record in validSet:
            userId, itemId, rating = record[0], record[1], record[2]
            if userId in usersInTrainSet and itemId in itemsInTrainSet:
                validSetToWrite.append(
                    str(userId) + " " + str(itemId) + " " + str(rating) + '\n')

        for record in testSet:
            userId, itemId, rating = record[0], record[1], record[2]
            if userId in usersInTrainSet and itemId in itemsInTrainSet:
                testSetToWrite.append(
                    str(userId) + " " + str(itemId) + " " + str(rating) + '\n')

        self.trainSize = len(trainSetToWrite)
        self.validSize = len(validSetToWrite)
        self.testSize = len(testSetToWrite)
        self.logger.info('trainSize: ' + str(self.trainSize))
        self.logger.info('validSize: ' + str(self.validSize))
        self.logger.info('testSize: ' + str(self.testSize))

        self.numUserAfterSplit = len(usersInTrainSet)
        self.numItemAfterSplit = len(itemsInTrainSet)
        self.logger.info('numUser after split: ' + str(self.numUserAfterSplit))
        self.logger.info('numItem after aplit: ' + str(self.numItemAfterSplit))

        fullOutputPath = self.inputPath + '/' + self.splitterType
        if not os.path.exists(fullOutputPath):
            os.makedirs(fullOutputPath)

        with open(fullOutputPath + '/train.txt', 'w') as trainFile:
            trainFile.writelines(trainSetToWrite)

        with open(fullOutputPath + '/valid.txt', 'w') as validFile:
            validFile.writelines(validSetToWrite)

        with open(fullOutputPath + '/test.txt', 'w') as testFile:
            testFile.writelines(testSetToWrite)

    def split_UserTimeRatio(self):

        random.seed(123)
        # collect all user-items mappings
        user_items = {}
        # collect all user item - rating review timeStamp mappings
        userItemToRatingTimeStamp = {}
        for record in self.wholeDataSet:
            userId, itemId, rating, timeStamp = record
            if userId not in user_items.keys():
                user_items[userId] = []
            user_items[userId].append([itemId, timeStamp])
            userItemToRatingTimeStamp[userId, itemId] = [rating, timeStamp]

        # split the data
        trainSet = []
        validSet = []
        testSet = []
        itemsInTrainSet = set()
        usersInTrainSet = set()
        for userIndex in self.userSet:
            # get and sort items
            items = sorted(user_items[userIndex], key=itemgetter(1))
            numItems = len(items)
            # split first n items into train set, except the last 2
            # items for trainSet
            for itemIndex, timeStamp in items[: -2]:
                rating, timeStamp = userItemToRatingTimeStamp[userIndex, itemIndex]
                trainSet.append([userIndex, itemIndex, rating, timeStamp])
                itemsInTrainSet.add(itemIndex)
                usersInTrainSet.add(userIndex)

                if userIndex not in self.user_rating_scale:
                    self.user_rating_scale[userIndex] = set()
                self.user_rating_scale[userIndex].add(rating)

            # items for validSet
            for itemIndex, timeStamp in items[-2:-1]:
                rating, timeStamp = userItemToRatingTimeStamp[userIndex, itemIndex]
                validSet.append([userIndex, itemIndex, rating, timeStamp])
                itemsInTrainSet.add(itemIndex)
                usersInTrainSet.add(userIndex)

                if userIndex not in self.user_rating_scale:
                    self.user_rating_scale[userIndex] = set()
                self.user_rating_scale[userIndex].add(rating)

            # items for testSet
            for itemIndex, timeStamp in items[-1:]:
                rating, timeStamp = userItemToRatingTimeStamp[userIndex, itemIndex]
                testSet.append([userIndex, itemIndex, rating, timeStamp])

        trainSetToWrite = []
        validSetToWrite = []
        testSetToWrite = []
        delete_count = 0

        for record in trainSet:

            userIndex, itemIndex, rating, timeStamp = record
            ratingSet = self.user_rating_scale[userIndex]

            if '5' not in ratingSet and '4' not in ratingSet and '3' not in ratingSet:
                continue
            if '1' not in ratingSet and '2' not in ratingSet:
                continue

            trainSetToWrite.append(
                str(userIndex) + " " + str(itemIndex) + " " + str(rating) + " "
                + str(timeStamp) + '\n')

        for record in validSet:
            userIndex, itemIndex, rating, timeStamp = record
            ratingSet = self.user_rating_scale[userIndex]

            if '5' not in ratingSet and '4' not in ratingSet and '3' not in ratingSet:
                continue
            if '1' not in ratingSet and '2' not in ratingSet:
                continue

            validSetToWrite.append(
                str(userIndex) + " " + str(itemIndex) + " " + str(rating) + " "
                + str(timeStamp) + '\n')

        for record in testSet:
            userIndex, itemIndex, rating, timeStamp = record
            ratingSet = self.user_rating_scale[userIndex]

            if '5' not in ratingSet and '4' not in ratingSet and '3' not in ratingSet:
                continue
            if '1' not in ratingSet and '2' not in ratingSet:
                continue

            if userIndex in usersInTrainSet and itemIndex in itemsInTrainSet:
                testSetToWrite.append(
                    str(userIndex) + " " + str(itemIndex) + " " + str(rating) + " "
                    + str(timeStamp) + '\n')
            else:
                delete_count += 1

        self.trainSize = len(trainSetToWrite)
        self.validSize = len(validSetToWrite)
        self.testSize = len(testSetToWrite)
        self.logger.info('trainSize: ' + str(self.trainSize))
        self.logger.info('validSize: ' + str(self.validSize))
        self.logger.info('testSize: ' + str(self.testSize))

        self.numRatingAfterSplit = self.numRatingBeforeSplit - delete_count
        self.logger.info('numRatingAfterSplit: ' + str(self.numRatingAfterSplit))
        self.numUserAfterSplit = len(usersInTrainSet)
        self.numItemAfterSplit = len(itemsInTrainSet)
        self.logger.info('numUser after split: ' + str(self.numUserAfterSplit))
        self.logger.info('numItem after aplit: ' + str(self.numItemAfterSplit))

        fullOutputPath = self.inputPath + '/' + self.splitterType
        if not os.path.exists(fullOutputPath):
            os.makedirs(fullOutputPath)

        with open(fullOutputPath + '/train.txt', 'w') as trainFile:
            trainFile.writelines(trainSetToWrite)

        with open(fullOutputPath + '/valid.txt', 'w') as validFile:
            validFile.writelines(validSetToWrite)

        with open(fullOutputPath + '/test.txt', 'w') as testFile:
            testFile.writelines(testSetToWrite)

    def buildModel(self):
        self.logger.info("\n###### information of DataModel ######\n")
        self.readData()

        self.generateEvalItemsForEachUser()

        self.printInfo()








