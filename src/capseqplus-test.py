
from src.recommender.CapSeqPlus import CapSeqPlusRecommender
from src.data_model.SequenceDataModel import SequenceDataModel
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"


if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 909,
        'testBatchSize': 909,
        'numFactor': 32,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 3,
        'target_length': 1,
        'hor_filter_num': 16,
        'ver_filter_num': 4,
        'dropout_keep': 0.8,
        'item_fc_dim': [32],
        'capsule_num': 1,
        'dynamic_routing_iter': 3,
        'eval_item_num': 100,
        'filter_num': 5,
        'early_stop': True,
        'loss': 'bpr',
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for input_len in [6, 7, 8, 9, 10]:
            for target_len in [1]:

                dataModel.generate_sequences_hor(input_len, target_len)

                for T in [3]:

                    for filter_num in [5]:
                        for capsule_num in [5]:

                            config['filter_num'] = filter_num
                            config['input_length'] = input_len
                            config['target_length'] = target_len
                            config['capsule_num'] = capsule_num

                            recommender = CapSeqPlusRecommender(dataModel, config)
                            recommender.run()



























