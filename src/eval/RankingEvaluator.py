from multiprocessing.pool import ThreadPool as Pool
import numpy as np
import time

class RankingEvaluator:

    def __init__(self, groundTruthLists, user_items_train, itemInTestSet, topK, testMatrix):

        '''
        :param groundTruthLists: a dict {userId:[itemId1, itemId2, ...], ...}
        :param user_items_train: a dict {userId:[itemId1, itemId2, ...], ...}
        :param itemInTestSet: Integer
        :param topK:    Integer
        :param testMatrix: a dict {(userId, itemId):rating ...}
        '''

        self.groundTruthLists = groundTruthLists
        self.user_items_train = user_items_train
        self.predLists = None
        self.indexRange = len(self.groundTruthLists)
        self.itemInTestSet = itemInTestSet
        self.topK = topK
        self.testMatrix = testMatrix
        self.pool = Pool()

    def setPredLists(self, predLists):
        self.predLists = predLists

    '''Recall'''
    def calRecall(self):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_Recall, (userIdx,)))

        recallSum = 0
        for result in results:
            recallSum += result.get()

        end = time.time()
        return recallSum / len(results), (end - start)

    def calculate_a_Recall(self, userIdx):
        hitNum = 0
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx]

        for itemId in userPredList:
            if itemId in userTrueList:
                hitNum += 1

        return hitNum / len(userTrueList)

    '''Precision'''
    def calPrecision(self):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_Precision, (userIdx,)))

        precisionSum = 0
        for result in results:
            precisionSum += result.get()

        end = time.time()

        return precisionSum / len(results), (end - start)


    def calculate_a_Precision(self, userIdx):
        hitNum = 0
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx]

        for itemId in userPredList:
            if itemId in userTrueList:
                hitNum += 1

        return hitNum / len(userPredList)

    '''AUC'''
    def calAUC(self):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_AUC, (userIdx,)))
        aucSum = 0
        for result in results:
            aucSum += result.get()

        end = time.time()

        return aucSum / len(results), (end - start)


    def calculate_a_AUC(self, userIdx):
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx]

        numEval = len(self.itemInTestSet) - len(self.itemInTestSet.intersection(set(self.user_items_train[userIdx])))

        numRelevant = 0
        numMiss = 0
        for itemIdx in userTrueList:
            if itemIdx in userPredList:
                numRelevant += 1
            else:
                numMiss += 1

        if numEval == numRelevant:
            return 1.0

        numPairs = numRelevant * (numEval - numRelevant)

        if numPairs == 0:
            return 0.5

        numHits = 0
        numCorrectPairs = 0
        for itemIdx in userPredList:
            if itemIdx in userTrueList:
                numHits += 1
            else:
                numCorrectPairs += numHits

        numCorrectPairs += numHits * (numEval - len(userPredList) - numMiss)
        return numCorrectPairs / numPairs

    '''NDCG'''
    def calNDCG(self):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_NDCG, (userIdx,)))
        ndcgSum = 0
        for result in results:
            ndcgSum += result.get()

        end = time.time()

        return ndcgSum / len(results), (end - start)


    def calculate_a_NDCG(self, userIdx):
        ratingList = []
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx]

        # calculate DCG and build rating list
        DCG = 0.0
        for index in range(len(userPredList)):
            itemIdx = userPredList[index]
            if itemIdx in userTrueList:
                ratingValue = self.testMatrix[userIdx, itemIdx]
                ratingList.append(ratingValue)
                DCG += ratingValue / np.log2(index + 2)
        if len(ratingList) == 0:
            return 0

        # calculate IDCG
        ratingList.sort(reverse=True)
        IDCG = 0.0
        for index in range(len(ratingList)):
            ratingValue = ratingList[index]
            IDCG += ratingValue / np.log2(index + 2)

        assert IDCG >= DCG
        return DCG / IDCG


if __name__ == '__main__':
    user_items_train = {0: [0, 1, 2, 3, 4, 5],
                        1: [0, 1, 3, 6],
                        2: [0, 2, 3, 6]}
    groundTruthList = {
                       0: [7, 8, 9],
                       1: [7, 8, 9],
                       2: [7, 8, 9]
    }
    topK = 3
    numItem = set()
    numItem.add(4)
    numItem.add(5)
    numItem.add(7)
    numItem.add(8)
    numItem.add(9)

    testMatrix = {(1, 7): 5,
                  (1, 8): 4,
                  (1, 9): 3,
                  (0, 7): 5,
                  (0, 8): 4,
                  (0, 9): 3,
                  (2, 7): 5,
                  (2, 8): 4,
                  (2, 9): 3,
                  }
    predLists = {0: [7, 8, 9],
                 1: [7, 4, 5],
                 2: [4, 7, 8],
                 }
    evaluator = RankingEvaluator(
        groundTruthLists=groundTruthList,
        user_items_train=user_items_train,
        itemInTestSet=numItem,
        topK=topK,
        testMatrix=testMatrix)
    evaluator.setPredLists(predLists)
    print("AUC:" + str(evaluator.calAUC()))
    print("Recall:" + str(evaluator.calRecall()))
    print("Precision:" + str(evaluator.calPrecision()))
    print("NDCG:" + str(evaluator.calNDCG()))









