from recommender.CCS_plus_user_item import CCSPlus_Recommender
from data_model.SequenceDataModel import SequenceDataModel

if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'test-info': 'ccs-plus-add-output-with-useritem-',
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 939,#830,
        'testBatchSize': 939, #830,
        'numFactor': 16,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 3,
        'target_length': 1,
        'dropout_keep': 0.8,
        'item_fc_dim': [16],
        'capsule_num': 1,
        'dynamic_routing_iter': 3,
        'eval_item_num': 100,
        'filter_num': 5,
        'early_stop': True,
        'capsule_num_1': 10,
        'capsule_num_2': 5,
        'capsule_num_3': 2,
        'pose_len': 16,
        'capsule_lambda': 0.01,
        'cnn_lam': 0.001,
        'mlp_lam': 0.001,
        'capsule_lam': 0.001,
        'save_model': False,
        'add_time': False,
        'time_user': False,
        'routing_type': 'em',
        'at_b': True,
        'at_c': True,
        'share_user_para_b': True,
        'share_user_para_c': True,
        'user_as_miu_bias': True,
        'user_reg_cost': False,
        'cluster_pretrain': False,
        'num_cluster': 10,
        'random_seed': 1,
        'rating_threshold': 100,

        'long-short-interact': 'cross',  # 'dot-product', 'cross', 'concat-mlp', 'outproduct-conv', 'self-attention'

        'corr-type': 'dot-product',  # 'dot-product', 'cross', 'concat-mlp', 'outproduct-conv'

        'out-product-filter-num': 2,
        'cross_layer_num': 3,

        'corr-cross-layer-num': 2,
        'corr-mlp-layer-sizes': [1],
        'corr-out-product-filter-num': 2,

        'item-weight-more': True,
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for input_len in [5]:
            for target_len in [1]:

                dataModel.generate_sequences_hor(input_len, target_len)
                config['input_length'] = input_len
                config['target_length'] = target_len

                for T in [3]:

                    for filter_num in [4]:
                        for capsule_num_1 in [4]:
                            for capsule_num_2 in [4]:
                                for capsule_num_3 in [4]:
                                    for at_b in [False]:
                                        for at_c in [False]:
                                            for share_b in [False]:
                                                for share_c in [False]:
                                                    for seed in [2]:
                                                        for threshold in [60]:
                                                            for item_weight_more in [True, False]:
                                                                for cross_layer_num in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
                                                                    config['item-weight-more'] = item_weight_more
                                                                    config['cross_layer_num'] = cross_layer_num
                                                                    config['num_cluster'] = 5
                                                                    config['rating_threshold'] = threshold
                                                                    config['random_seed'] = seed
                                                                    config['at_b'] = at_b
                                                                    config['at_c'] = at_c
                                                                    config['share_user_para_b'] = share_b
                                                                    config['share_user_para_c'] = share_c
                                                                    config['capsule_num_1'] = capsule_num_1
                                                                    config['capsule_num_2'] = capsule_num_2
                                                                    config['capsule_num_3'] = capsule_num_3
                                                                    config['filter_num'] = filter_num
                                                                    if at_b == False and share_b == True:
                                                                        continue
                                                                    if at_c == False and share_c == True:
                                                                        continue
                                                                    recommender = CCSPlus_Recommender(dataModel, config)
                                                                    recommender.run()
